import time
import glob
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

path = "/home/raulsanchez/recsys_airbnb/"
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")

driver = webdriver.Firefox()
driver.get("https://www.airbnb.com")
driver.find_element_by_css_selector(".show-logout [href='/login']").click();time.sleep(4)
driver.find_element_by_id("signin_email").send_keys("dabirolbd@gmail.com")
driver.find_element_by_id("signin_password").send_keys("c5h1yyugr"); time.sleep(2)
driver.find_element_by_id("user-login-btn").click()#login

sw_lat = 21.28612205124835
sw_lng = -157.8688530644314
ne_lat = 21.327852208179984
ne_lng = -157.82713934983178
dist_lat_range = abs(sw_lat-ne_lat)
dist_lng_range = abs(sw_lat-ne_lat) #4 kilometers
base_url = "https://www.airbnb.com/s/?zoom=14&search_by_map=true&"

for room_id in reversed(test_cases.target_room.unique()):
#for room_id in test_cases.target_room.unique():
    #room_id = test_cases.target_room.unique()[0]
    print(room_id)
    room = cities[cities.id == room_id].iloc[0]
    
    sw_lat = str( room.latitude - dist_lat_range/2 )
    ne_lat = str( room.latitude + dist_lat_range/2 )
    sw_lng = str( room.longitude - dist_lng_range/2 )
    ne_lng = str( room.longitude + dist_lng_range/2 )
    url = base_url+"sw_lat="+sw_lat+"&sw_lng="+sw_lng+"&ne_lat="+ne_lat+"&ne_lng="+ne_lng
    driver.get(url)
    time.sleep(10)
    body = driver.find_element_by_css_selector("body").get_attribute('innerHTML')
    with open("/home/raulsanchez/city_context/"+ str(room.id[4:]) + ".html", 'w', encoding='utf-8') as f:
        print(body, file=f)    
driver.close()
import subprocess
import glob

path ='/home/raul/airbnb2/'
result_files = glob.glob(path+"/results/*")
result_files.sort()

baseline_exp_1 = ""
baseline_exp_2 = ""
qrel_exp_1 = ""
qrel_exp_2 = ""
results_exp_1 = []
results_exp_2 = []

for file in result_files:
    #full_path = result_files[0]
    file = file.replace("//","/")

    if(file.find("qrel") != -1 ):
        if( (qrel_exp_1 == "") & (file.find("exp_1") == -1)):
            qrel_exp_1 = file
        if((qrel_exp_2== "") & (file.find("exp_2") == -1)):
            qrel_exp_2= file
    else:
        if(file.find("old") == -1):
            if(file.find("exp_1") == -1):
                if(baseline_exp_2 == ""):
                    baseline_exp_2 = file
                else:                
                    results_exp_2.append(file)
            else:
                if(baseline_exp_1 == ""):
                    baseline_exp_1 = file
                else:
                    results_exp_1.append(file)
print("EXPERIMENT 1")
i=0
for result in results_exp_1:
    params=["/home/raul/trec_eval.9.0/sigeval"]
    params.append(qrel_exp_1)
    params.append(baseline_exp_1)
    params.append(result)
    params.append("-t")
    params.append("/home/raul/trec_eval.9.0/trec_eval")
    params.append("-f")
    params.append("--")
    params.append("-m")
    params.append("recip_rank")
    out = subprocess.check_output(["perl"]+params)
    print(str(out).replace("\\n"," \n "))
    print("----------------------")
    i=i+1

print("EXPERIMENT 2")
i=0
for result in results_exp_2:
    params=["/home/raul/trec_eval.9.0/sigeval"]
    params.append(qrel_exp_2)
    params.append(baseline_exp_2)
    params.append(result)
    params.append("-t")
    params.append("/home/raul/trec_eval.9.0/trec_eval")
    params.append("-f")
    params.append("--")
    params.append("-m")
    params.append("recip_rank")
    out = subprocess.check_output(["perl"]+params)
    print(str(out).replace("\\n"," \n "))
    print("----------------------")
    i=i+1
import pandas as pd
import numpy as np

path ='/home/raul/recsys_airbnb/'
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York","San_Francisco", "Los_Angeles","Chicago","Boston"]

cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
ranking_listings = pd.read_csv(path+'datasets/ranking_listings.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

cities_reviews = pd.read_csv(path+'datasets/cities_reviews.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
world_reviews = pd.read_csv(path+'datasets/world_reviews.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
ranking_listings_reviews = pd.read_csv(path+'datasets/ranking_listing_reviews.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

test_cases = pd.read_csv(path+'datasets/test_cases.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
user_item_matrix2 = pd.read_csv(path+'datasets/user_item_matrix_airbnb.csv', sep="\t", header=0, na_values="NaNxxxx")
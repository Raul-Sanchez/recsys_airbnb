import re
import glob
import string
import pandas as pd
import numpy as np
from pyquery import PyQuery as pq

error_urls = pd.read_csv("/home/raul/Desktop/room_with_no_context_histogram.csv", header=-1)
world = pd.read_csv('/home/raul/airbnb2/datasets/world_with_context_v1.1.csv', sep="\t", header=0, na_values="NaNxxxx")
new_features = ["context_mean","context_min", "context_max", "context_histogram", "context_number_of_rooms","context_names", "context_prices", "context_ratings", "context_number_of_reviews", "context_room_type", "context_room_id", "context_host_id", "context_instant_booking", "context_super_hosts"]
error_rooms = list()
for room_id_raw in error_urls.values:
    #room_id = error_urls.values[1]
    print(room_id_raw)
    room_id = room_id_raw[0]
    html = pq(filename="/home/raul/Desktop/rooms/"+room_id[4:]+".html")
    
    context_mean = html(".avg-price").text()
    if(context_mean == ""):
        error_rooms.append(room_id)
    else:
        context_mean = int(''.join(filter(lambda x: x.isdigit(), context_mean)))
    
        context_min = pq(html(".price-range-slider .row .price")[0]).text()
        context_min = int( context_min )
    
        context_max = pq(html(".price-range-slider .row .price")[1]).text()
        context_max = int(''.join(filter(lambda x: x.isdigit(), context_max)))
    
        context_histogram = list()
        for item in html(".p2-histogram .p2-histogram-bar"):
            style_str = pq(item).attr.style
            style_str = style_str[style_str.index("height")+7:]
            height = float(style_str[:style_str.index(";")-2])
            context_histogram.append( height )
        #pd.DataFrame(context_histogram).plot(kind="area")
    
        context_number_of_rooms = html(".results_count").text()
        if("1 of 1 Rental" in context_number_of_rooms):
            context_number_of_rooms = 1
        else:
            ini = context_number_of_rooms.index("of")
            end = context_number_of_rooms.index("Rentals")
            context_number_of_rooms = context_number_of_rooms[ini+2:end]
            index2 = re.search("\+", context_number_of_rooms)
            if(index2):
                context_number_of_rooms = 301
            else:
                context_number_of_rooms = int(context_number_of_rooms)            

        context_names = list()
        context_prices = list()
        context_ratings = list()
        context_number_of_reviews = list()
        context_room_type = list()
        context_room_id = list()
        context_host_id = list()
        context_instant_booking = list()
        context_super_hosts = list()
        for item in html(".listings-container .col-md-6"):
            #item = html(".listings-container .col-md-6")[7]
            item = pq(item)
            
            name = item(".listing-name").text()
            context_names.append(name)
            
            price =  item(".price-amount").text()
            context_prices.append(int(price))
            
            full_start = len(item(".foreground .icon-star"))
            half_start = len(item(".foreground .icon-star-half"))
            context_ratings.append(full_start + half_start*0.5)
         
            number_reviews = item(".listing-location").text()
            index_2 = re.search("\d", number_reviews)
            if index_2:
                i = index_2.start()
                room_type = number_reviews[:i]
                number_reviews = number_reviews[i: ]
    
                number_reviews = int(number_reviews[: number_reviews.index(" ")])            
                room_type = room_type[:room_type.index("\n")].replace(" ","_")
            else:
                room_type = number_reviews.replace(" ","_")
                number_reviews = 0
    
            context_number_of_reviews.append(number_reviews)
            context_room_type.append(room_type)
            
            room_id = item(".listing-img > a").attr.href        
            room_id = int(room_id[7:room_id.index("?")])
            context_room_id.append(room_id)
            
            host_id = item(".card-profile-picture").attr.href
            host_id = int(host_id[12:])
            context_host_id.append(room_id)
            
            if len(item(".icon-instant-book")) == 0:
                context_instant_booking.append(False)
            else:
                context_instant_booking.append(True)
            
            if (len(item(".superhost-photo-badge")) == 0 ):
                context_super_hosts.append(False)
            else:
                context_super_hosts.append(True)
        
        room = world[world.id == room_id_raw[0]]
        room.context_mean = context_mean
        room.context_min = context_min
        room.context_max = context_max
        room.context_histogram = str(context_histogram) # pd.Series(context_histogram).plot(kind="bar")
        room.context_number_of_rooms = context_number_of_rooms
        room.context_names = str(context_names)
        room.context_prices = str(context_prices)
        room.context_ratings = str(context_ratings)
        room.context_number_of_reviews = str(context_number_of_reviews)
        room.context_room_type = str(context_room_type)
        room.context_room_id = str(context_room_id)
        room.context_host_id = str(context_host_id)
        room.context_instant_booking = str(context_instant_booking)
        room.context_super_hosts = str(context_super_hosts)

        world[world.id == room_id_raw[0]] = room
world.to_csv('/home/raul/airbnb2/datasets/world_with_context_v1.3.csv', sep="\t", header=world.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
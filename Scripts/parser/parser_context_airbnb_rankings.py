import re
import glob
import string
import pandas as pd
import numpy as np
from pyquery import PyQuery as pq
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York","San_Francisco", "Los_Angeles","Chicago","Boston"]
path="/home/raul/recsys_airbnb/"
ranking_listings = pd.read_csv(path+"datasets/ranking_listings_v1.3(num_of_rev).csv", sep="\t", header=0, na_values="NaNxxxx")
cities = pd.read_csv(path+"datasets/cities_v1.2.csv", sep="\t", header=0, na_values="NaNxxxx")
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)

new_features = ["context_mean","context_min", "context_max", "context_histogram", "context_number_of_rooms","context_names", "context_prices", "context_ratings", "context_number_of_reviews", "context_room_type", "context_room_id", "context_host_id", "context_instant_booking", "context_super_hosts"]
for f in new_features:
    ranking_listings[f] = [0]*len(ranking_listings)
#len(test_cases.target_room.unique())
for target_room in test_cases.target_room.unique():
    print(target_room)
    test_case = test_cases[test_cases.target_room == target_room].iloc[0]
    rooms = ranking_listings[ranking_listings.id.isin(test_case.airbnb_ranking)]
    target_room = cities[cities.id == test_case.target_room]
    
    rooms.context_mean = [target_room.context_mean.values[0]] * len(rooms)
    rooms.context_min = [target_room.context_min.values[0]] * len(rooms)
    rooms.context_max = [target_room.context_max.values[0]] * len(rooms)
    rooms.context_histogram = list(target_room.context_histogram) * len(rooms)
    rooms.context_number_of_rooms = [target_room.context_number_of_rooms.values[0]] * len(rooms)
    rooms.context_names = list(target_room.context_names) * len(rooms)
    rooms.context_prices = list(target_room.context_prices) * len(rooms)
    rooms.context_ratings = list(target_room.context_ratings) * len(rooms)
    rooms.context_number_of_reviews = list(target_room.context_number_of_reviews) * len(rooms)
    rooms.context_room_type = list(target_room.context_room_type) * len(rooms)
    rooms.context_room_id = list(target_room.context_room_id) * len(rooms)
    rooms.context_host_id = list(target_room.context_host_id) * len(rooms)
    rooms.context_instant_booking = list(target_room.context_instant_booking) * len(rooms)
    rooms.context_super_hosts = list(target_room.context_super_hosts) * len(rooms)
    ranking_listings[ranking_listings.id.isin(test_case.airbnb_ranking)] = rooms
var fs = require('fs');
var cheerio = require('cheerio');
var async = require('async');
var walk    = require('walk');
var parser = {
    history : [],
    city: "Los_Angeles",
    records: [],
    total_reviews: [],
    total_rooms: [],
    error_room_id:[],
    room_scheme: require("./room_scheme"),
    schema: require("./schema.js"),
    overlook : ["monthly_price","weekly_price","bed_type", "extra_people","scrape_id","last_scraped","experiences_offered","neighborhood_overview","thumbnail_url","medium_url","picture_url","xl_picture_url","host_acceptance_rate","host_thumbnail_url","host_picture_url","host_neighbourhood","host_listings_count","host_total_listings_count","host_verifications","host_has_profile_pic","neighbourhood_cleansed","neighbourhood_group_cleansed","city","zipcode","market","smart_location","country_code","is_location_exact","square_feet","security_deposit","cleaning_fee","guests_included","maximum_nights","calendar_updated","has_availability","availability_30","availability_60","availability_90","availability_365","calendar_last_scraped","first_review","last_review","requires_license","license","jurisdiction_names","instant_bookable","require_guest_profile_picture","require_guest_phone_verification","calculated_host_listings_count","reviews_per_month"
        ,"review_scores_rating",
        "review_scores_accuracy",
        "review_scores_cleanliness",
        "review_scores_checkin",
        "review_scores_communication",
        "review_scores_location",
        "review_scores_value"
    ],
    err: function(id, msg){ console.log("ERROR "+id +": "+msg);},
    save_error_ids: function(){
        filepath = "/media/raul/BACKUP/airbnb_rankings/"+parser.city+'/error_url.csv';
        fs.writeFile(filepath, parser.error_room_id, function(err) {
          if (err) throw err;
          //console.log("----------"+filepath);
        });
    },
    init:function(){
        parser.readGuestRecords(function(){
            console.log(parser.records.length)
            var i = 0
            async.eachSeries(parser.records, function (record, callback){
            //async.eachSeries(parser.records.slice(12570, parser.records.length), function (record, callback){
                //console.log("--------"+record)
                parser.parserRoom(record, callback);                
                console.log(i)
                i+=1;
            }, function (err) {
                parser.save_error_ids()
            });

        });        
    },
    parserRoom:function(file, callback){
        try {
            var $ = cheerio.load(fs.readFileSync(file));
            
            if( $("h1.text-jumbo").length == 0 ){
                //console.log(file)
                parser.extractReviews($, file, callback);
            }else{
                parser.err("1", "Error Stay tuned");
                callback();
            }
        } catch (e) {
            console.log(e)
            parser.err("66", "File not found");
            callback();
        }
    },
    recordParser: function(str){
        str = str.substring(29, str.length);
        index = str.indexOf(",")-1;
        room_id = str.substring(0, index);
        
        str = str.substring(index+3, str.length);
        index = str.indexOf(",");
        guest_id = str.substring(0, index-1);
        if(str.indexOf("\r") == -1)
            host_id = str.substring(index+2, str.length);
        else
            host_id = str.substring(index+2, str.length-1);
        record = {room_id: room_id, guest_id: guest_id, host_id: host_id};
        parser.records.push( record );
    },
    readGuestRecords: function(callback){
        var walker  = walk.walk('/media/raul/BACKUP/airbnb_rankings/'+parser.city+"/error_rooms", { followLinks: false });

        walker.on('file', function(root, stat, next){
            path = root + '/' + stat.name
            parser.records.push(path);
            next();
        });        
        walker.on('end', function() {
            callback()
        });
    },
    extractReviews: function($$, file, callback){
        var reviews = $$(".review");
        var cb = callback;
        var $ = $$;
        var total_reviews = "";
        var f = file;

        //save_as = file.slice(46+parser.city.length, file.length-5 )
        save_as = file.slice(48+parser.city.length, file.length-5 )
        console.log(save_as)
        async.eachSeries(reviews, function (item, next) {
            var review = {};
            var $name = $(item).find(".name a");
            var href = $name.attr("href");
            //listing_url,id,date,reviewer_id,reviewer_name,comments
            path_length = ("/media/raul/BACKUP/airbnb_rankings/rooms_new/".length)
            review.listing_id = f.slice(path_length+parser.city.length, file.length-5 )
            review.id = "NaN";
            review.date = $(item).find(".date").text().trim();
            review.reviewer_id = href.substring(12, href.length);
            review.reviewer_name = $name.text().trim();
            review.comments = $(item).find("p").text().trim().replace(/(\r\n|\n|\r)/gm,"");

            var errors = [];
            for(var key in review){
                if(review[key] == "")                   
                        errors.push(key)
            }
            if(errors.length > 0){
                parser.err("14", " missin "+ errors +" "+ review.listing_url);       
            }else{
                parser.singleLine(review, function(str){
                    if(total_reviews == undefined)
                        total_reviews = "";
                    
                    total_reviews += str +"\n";
                    next();
                });
            }
        }, function (err) {
            //callback();
            parser.writeReview(save_as, total_reviews);
            parser.getJSONRoom($, save_as, callback);
        });//*/
    },
    writeReview: function(room_id, total_reviews){
        filepath = "/media/raul/BACKUP/airbnb_rankings/"+parser.city+'/rooms_new/reviews/reviews_'+ room_id +'.csv';
        //console.log("----------"+total_reviews);
        fs.writeFile(filepath, total_reviews, function(err) {
          if (err) throw err;
          //console.log("----------"+filepath);       
        });
    },
    singleLine: function(myobj, n){
        var myobject = myobj;
        var cb = n;
        var str = "";
        async.forEachOf(myobject, function (value, key, next) {
            myobject[key] = myobject[key].replace(/(\r\n|\n|\r)/gm,"");
            str+=myobject[key]+" | ";
            next();
        }, function () {
            str = str.substring(0, str.length-2); //removing last |
            str.replace(/(\r\n|\n|\r)/gm,"");
            //setTimeout(function(){}, 10000)
            cb(str)
        });
    },
    writeRoom: function(room){

        var file = fs.createWriteStream(filepath);
        parser.total_rooms.forEach(function(i){
            file.write(parser.singleLine(i)+"\n");
        });
        file.end();
    },
    getJSONRoom: function($, record, callback){
        var callback = callback;
        var error_stay_tuned = false;
        var room = JSON.parse( JSON.stringify( parser.room_scheme ) );
        room.id                 = record
        room.listing_url        = "https://wwww.airbnb.com/rooms/"+record;
        room.property_type      = $($("#details .col-md-9 a>strong")[0]).text();
        not_found_got_redirected =  $("input").length > 50 
        if(!not_found_got_redirected){
        if( !isNaN(parseFloat(room.property_type)) )
            room.property_type      = $($("#details .col-md-9 a>strong")[1]).text();
        if( !isNaN(parseFloat(room.property_type)) )
            parser.err("500", "property_type "+ record);
        //console.log(room.property_type)
        room.name               = $("h1").text();
        $summary                = $("div>p>span")
        if($summary.length !=0){
            if($summary[0].children.length != 0){
                room.summary = $summary[0].children[0].data;                
            }else{
                parser.err("3", "Error Stay tuned "+ record);
                error_stay_tuned = true;
            }
        }else{
            parser.err("2", "Error Stay tuned "+ record);
            error_stay_tuned = true;
        }
        

        if(!error_stay_tuned){ 
        if(room.summary == "")
            room.summary = $($("div>p>span")[1]).text();
        room.description        = room.summary + " ";

        $description            = $(".description .expandable-content>[data-reactid]")
        room.space              = $($description[0]).find("p>span").text();
        room.description        += room.space;
        room.description        += $($description[1]).find("p>span").text()+" ";
        room.description        += $($description[2]).find("p>span").text()+" ";
        
        var find = '\"';
        var re = new RegExp(find, 'g');
        room.description = room.description.replace(re, " ");

        room.transit            = $($description[3]).find("p>span").text();
        if( room.transit == "" )
            room.transit = "nan";

        room.notes              = $($description[4]).find("p>span").text();
        if( room.notes == "" )
            room.notes = "nan";

        $host = $("#host-profile")
        room.host_url           = $host.find(".media-photo").attr("href");
        if(room.host_url != undefined)
            room.host_url.trim();
        else
            room.host_url = "";
        room.host_id            = room.host_url.substring(12, room.host_url.length );
        room.host_name          = $host.find("h4").text().trim();
        if(room.host_name != "Your Host"){
            room.host_name          = room.host_name.substring(16, room.host_name.length);
        }else{
            room.host_name = $host.find("h3").text().trim();
        }
        $host_details           = $host.find(".col-md-6>div")
        room.host_since         = $( $host_details[1] ).text().trim();
        room.host_location      = $( $host_details[0] ).text().trim();
        if( room.host_location.indexOf("Response time") != -1){
            room.host_location = $($host.find(".col-md-9 .space-2 .text-muted span")[0]).text()
        }
        
        room.host_response_time = $( $host.find(".col-md-6 strong")[1] ).text();
        if(room.host_response_time == "")
            room.host_response_time = "nan";
        
        room.host_response_rate = $( $host.find(".col-md-6 strong")[0] ).text();        
        if(room.host_response_rate == "")
            room.host_response_rate = "nan";

        room.street             = $("#hover-card .panel-body .listing-location").text();
        room.neighbourhood      = $("h3.seo-link").text();
        if( room.neighbourhood == "")
            room.neighbourhood = $(".h3>a").text();
        if( room.neighbourhood == "")
            room.neighbourhood = $(".seo-link a").text();
        if( room.neighbourhood == "" )
            room.neighbourhood = $($(".listing-location>a.text-muted>span")[1]).text();
        if( room.neighbourhood == "" )
            room.neighbourhood = "nan";

        $address_details =  $("#hover-card .panel-body .listing-location")      
        room.country            = $( $address_details[$address_details.length-1] ).text();
        room.state              = $( $address_details[$address_details.length-2] ).text();
        if(room.state == "" && room.country != "")
            room.state = "nan"
        room.price =  $(".book-it .text-special").text().trim().replace(/\$/g, '');
        if(/^\d+$/.test(room.price)){
            room.price = parseFloat(room.price)
        }else{
            room.price =  $(".book-it .text-special").text().trim().replace(/\$/g, '');
            parser.error_room_id.push(room.listing_url);
            parser.err("4321","found price "+ room.price+"  "+room.id);
        }

        room.host_about         = $("#host-profile .expandable-content>p").text().trim();       
        if(room.host_about == "")
            room.host_about = $("#host-profile .expandable-content p").text();
        if( room.host_about == "")
            room.host_about = "nan";
        
        room.minimum_nights     = $("#details-column .col-md-6>strong:not([data-reactid]) ").text();
        if(room.minimum_nights == "")
            room.minimum_nights = "null";

        $("meta[content]").each(function(){
            str = $(this).attr("content");
            ini = str.indexOf("offset_lat");
            if ( ini != -1 ){
                str = str.substring(ini+12, 200 );
                end = str.indexOf("}");
                str = str.substring(0, end)
                middle = str.indexOf(",");
                lat = str.substring(0, middle);
                lng = str.substring(middle+14, str.length)

                room.latitude = lat;
                room.longitude = lng;
            }
        });
        var error = true;
        room.amenities = [];
        $(".amenities .col-sm-6 strong").each(function(){
            error = false
            var amenity = $(this).text().trim();
            if(amenity != "+ More")
                room.amenities.push(amenity);
        });
        $details = $("#details-column .row[data-reactid] .col-md-9 .col-md-6>div strong");
        $("#details-column .row[data-reactid] .col-md-9 .col-md-6>div").each(function(){
            $divs = $(this).children();
            feature_str = $($divs[0]).text();
            f_semicolon = feature_str.indexOf(":");
            feature = feature_str.slice(0, f_semicolon).trim();
            value = feature_str.slice(f_semicolon, feature_str.length).trim();
            switch( feature ){
                case "Room type:":
                    room.room_type = value;
                    break;
                case "Bed type:":
                    room.bed_type = value;
                    break;
                case "Accommodates:":
                    room.accommodates = value;
                    break;
                case "Bedrooms:":
                    room.bedrooms  = value;
                    break;
                case "Bathrooms:":
                    room.bathrooms = value;
                    break;
                case "Beds:":
                    room.beds = value;
                    break;
                case "Check Out:":
                  room.check_out = value;
                 break;
                case "Check In:":
                  room.check_in = value;
                 break;
                case "Pet Owner:":
                    room.amenities.push(value)
                    break;
                case "Extra people:":
                    room.extra_people  = value.replace(/\$/g, '');
                    break;
                case "Weekly Price:":
                    room.weekly_price  = value.replace(/\$/g, '');
                    break;
                case "Monthly Price:":
                    room.monthly_price = value.replace(/\$/g, '');
                    break;
                case "Weekly discount:":
                    value = parseInt( value.replace(/\%/g, '') )
                    room.weekly_price  = room.price-(room.price*0.01)*value;
                    break;
                case "Monthly discount:":
                    value = parseInt( value.replace(/\%/g, '') )
                    room.monthly_price = room.price-(room.price*0.01)*value;
                    break;
                case "Cancellation:":
                    room.cancellation_policy = value;
                    break;
                case "Security Deposit:":
                    room.security_deposit  = value.replace(/\$/g, '');
                    if(/^\d+$/.test(room.security_deposit)){
                        room.security_deposit = parseFloat(room.security_deposit)
                    }else{
                        parser.error_room_id.push(room.listing_url);
                        parser.err("4321","found security_deposit "+ room.security_deposit+"  "+room.id);
                    }
                    break;
                case "Cleaning Fee:":
                    room.cleaning_fee = value.replace(/\$/g, '');
                    if(/^\d+$/.test(room.cleaning_fee)){
                        room.cleaning_fee = parseFloat(room.cleaning_fee)
                    }else{
                        parser.error_room_id.push(room.listing_url);
                        parser.err("4321","found cleaning_fee "+ room.cleaning_fee+"  "+room.id);
                    }
                    break;
                default:                            
                break;
            }
        });
        if(room.room_type == ""){ //second parser
            $(".row>.col-md-9 .row .col-md-6 > [data-reactid]").each(function(){
                feature_str = $(this).text();

                f_semicolon = feature_str.indexOf(":");
                feature = feature_str.slice(0, f_semicolon+1).trim();
                value = feature_str.slice(f_semicolon+1, feature_str.length).trim();
                switch(feature){
                    case "Room type:":
                        room.room_type = value;
                    break;
                    case "Bed type:":
                        room.bed_type = value;
                        break;
                    case "Accommodates:":
                        room.accommodates = value;
                        break;
                    case "Bedrooms:":
                        room.bedrooms  = value;
                        break;
                    case "Bathrooms:":
                        room.bathrooms = value;
                        break;
                    case "Beds:":
                        room.beds = value;
                        break;
                    case "Check Out:":
                       room.check_out = value;
                     break;
                    case "Check In:":
                          room.check_in = value;
                    break;
                    case "Pet Owner:":
                        room.amenities.push(value)
                        break;
                    case "Extra people:":
                        room.extra_people  = value;
                        break;
                    case "Weekly Price:":
                        room.weekly_price  =value;
                        break;
                    case "Monthly Price:":
                        room.monthly_price = value;
                        break;
                    case "Weekly discount:":
                        value = parseInt( value.replace(/\%/g, '') )
                        room.weekly_price  = room.price-(room.price *0.01)*value;
                    break;
                    case "Monthly discount:":
                        value = parseInt( value.replace(/\%/g, '') )
                        room.monthly_price = room.price-(room.price*0.01)*value;
                    break;
                    case "Cancellation:":
                        room.cancellation_policy = value;
                        break;
                    case "Security Deposit:":
                        room.security_deposit  = value.replace(/\$/g, '');
                        if(/^\d+$/.test(room.security_deposit)){
                            room.security_deposit = parseFloat(room.security_deposit)
                        }else{
                            parser.error_room_id.push(room.listing_url);
                            parser.err("4321","found security_deposit "+ room.security_deposit+"  "+room.id);
                        }
                        break;
                    case "Cleaning Fee:":
                        room.cleaning_fee = value.trim().replace(/\$/g, '');
                        if(/^\d+$/.test(room.cleaning_fee)){
                            room.cleaning_fee = parseFloat(room.cleaning_fee)
                        }else{
                            parser.error_room_id.push(room.listing_url);
                            parser.err("4321","found cleaning_fee "+ room.cleaning_fee+"  "+room.id);
                        }
                        break;
                    default:                            
                    break;
                        
                }
            });
        }
        if(room.cancellation_policy == "")
            room.cancellation_policy =$($(".book-it__message-text strong")[5]).text().trim();
        if(room.accommodates == ""){
            room.accommodates = $($("#details-column .row[data-reactid] .col-md-9 .col-md-6>div")[2]).find("strong").text();
        }
        room.host_is_superhost  = "f";
        room.host_identity_verified ="f";
        $("#host-profile .col-lg-8 .col-md-9").each(function(){
            str = $(this).text().trim();

            superhost = str.indexOf("erhost")
            if(superhost != -1)
                room.host_is_superhost  = "t";
            
            reviews = str.indexOf("eview")
            if(reviews != -1)
                room.number_of_reviews = str.match(/\d+/)[0];
            
            verified = str.indexOf("erified")
            if(verified != -1)
                room.host_identity_verified ="t"
        })
        //console.log(record.listing_url)
        
        var $ratings = $(".review-wrapper .text-beach")     

        room.review_scores_rating = $($ratings[0]).text() *20 //overall
        room.review_scores_accuracy = $($ratings[1]).text() //accuracy
        room.review_scores_communication = $($ratings[2]).text() //communication    
        room.review_scores_cleanliness = $($ratings[3]).text() //cleanliness
        room.review_scores_location = $($ratings[4]).text()//location
        room.review_scores_checkin = $($ratings[5]).text()//checkin
        room.review_scores_value = $($ratings[6]).text()//value
        
        if(room.review_scores_accuracy == ""){
            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[0]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[0]).find(".icon-star.icon.icon-beach").length
            room.review_scores_rating = (stars+half_stars)*20

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[1]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[1]).find(".icon-star.icon.icon-beach").length
            room.review_scores_accuracy = stars+half_stars

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[2]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[2]).find(".icon-star.icon.icon-beach").length
            room.review_scores_communication = stars+half_stars

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[3]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[3]).find(".icon-star.icon.icon-beach").length
            room.review_scores_cleanliness = stars+half_stars

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[4]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[4]).find(".icon-star.icon.icon-beach").length
            room.review_scores_location = stars+half_stars

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[5]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[5]).find(".icon-star.icon.icon-beach").length
            room.review_scores_checkin = stars+half_stars

            half_stars = $($(".review-wrapper .star-rating-wrapper .foreground")[6]).find(".icon-star-half").length*0.5
            stars = $($(".review-wrapper .star-rating-wrapper .foreground")[6]).find(".icon-star.icon.icon-beach").length
            room.review_scores_value = stars+half_stars
        }   
        if(room.review_scores_rating == ""){
            console.log("WARNING: no ratings")
        }
        //console.log(room)
        var errors = [];
        for(var key in room){
            if(room[key] == "" || room[key] == undefined || room[key] == "undefined" ){
                if( parser.overlook.indexOf(key) == -1 && key != "amenities"){
                        errors.push(key)
                }               
            }
        }
        
        if( error ){
            errors.push("amenities")
        }
        var server_not_found = ($("h1").text().trim() == "Server not found");
        if(server_not_found){
            parser.err("123", "Error Server not found "+ record);
            parser.error_room_id.push(room.listing_url);
        }

        var room_not_found = $('[for="map-search-min-bedrooms"]').length == 1;
        if(room_not_found){
            parser.err("12345", "Room not found, instead search a new one: "+ record);
        }

        if(errors.length > 0 &&  !server_not_found && !room_not_found){
            parser.err("5", " missin "+ errors +" "+ record);
        }

        var filepath = "/media/raul/BACKUP/airbnb_rankings/"+parser.city+'/rooms_new/rooms/room_'+ room.id +'.csv';
        var str = ""
        
        async.forEachOf(room, function (value, key, next) {
            value = JSON.stringify(value); //transforming objects to string (specially arrays :D )
            value = value.replace(/\|/g, ' ');
            value = value.replace(/\"/g, '');
            
            if (value == '')
                value = "NaN";
            str += value + " | "; // R csv separator
            next();
        }, function (err) {
            str = str.substring(0, str.length-1);
            str += "\n";
            //console.log(filepath);
            fs.writeFile(filepath, str , function(err) {
                if (err) throw err;
                
                callback();
            });
        });//*/
        }else{
            callback();
        }
        }else{
            parser.err("3", "Warning not available any longer "+ record);
            callback();
        }
    }
}
parser.init();
import re
import glob
import string
import pandas as pd
import numpy as np
from pyquery import PyQuery as pq

#www.airbnb.com/rooms/4163834.html
#https://www.airbnb.com/s/?zoom=14&search_by_map=true&sw_lat=50.73567845773418&sw_lng=-2.9712261506558177&ne_lat=50.777408614665816&ne_lng=-2.9294959937241827
#/home/raul/airbnb/Boston/context/2546894.html
#https://www.airbnb.com/s/?zoom=14&search_by_map=true&sw_lat=34.22398021113418&sw_lng=-116.91482272346582&ne_lat=34.26571036806582&ne_lng-116.87309256653418

city_str = "Boston"
path ='/home/raul/airbnb/'+city_str+'/'
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx")
#new_features = ["context_mean","context_min", "context_max", "context_histogram"
#,"context_shared_room"]
#for f in new_features:
#    world[f] = np.nan

allFiles = glob.glob(path+"context/*.html")

for file in allFiles:
    #file = "/home/raul/airbnb/Boston/context/2546894.html"
    room_id = int(file[(27+len(city_str)):-5])
    #room = world[world.id == room_id]
    html = pq(filename=file)

    context_mean = html(".avg-price").text()
    context_mean = int(''.join(filter(lambda x: x.isdigit(), context_mean)))
    
    context_min = pq(html(".price-range-slider .row .price")[0]).text()
    context_min = int( context_min )
    
    context_max = pq(html(".price-range-slider .row .price")[1]).text()     
    context_max = int(''.join(filter(lambda x: x.isdigit(), context_max)))
    
    context_histogram = list()
    for item in html(".p2-histogram .p2-histogram-bar"):
        style_str = pq(item).attr.style
        style_str = style_str[style_str.index("height")+7:]
        height = float(style_str[:style_str.index(";")-2])
        context_histogram.append( height )
    #pd.DataFrame(context_histogram).plot(kind="area")

    context_number_of_rooms = html(".results_count").text()
    ini = context_number_of_rooms.index("of")
    end = context_number_of_rooms.index("Rentals")
    context_number_of_rooms = context_number_of_rooms[ini+2:end]
    index2 = re.search("\+", context_number_of_rooms)
    if(index2):
        context_number_of_rooms = 301
    else:
        context_number_of_rooms = int(context_number_of_rooms)

    context_names = list()
    context_prices = list()
    context_ratings = list()
    context_number_of_reviews = list()
    context_room_type = list()
    context_room_id = list()
    context_host_id = list()
    context_instant_booking = list()
    context_super_hosts = list()
    for item in html(".listings-container .col-md-6"):
        #item = html(".listings-container .col-md-6")[7]
        item = pq(item)
        
        name = item(".listing-name").text()
        context_names.append(name)
        
        price =  item(".price-amount").text()
        context_prices.append(int(price))
        
        full_start = len(item(".foreground .icon-star"))
        half_start = len(item(".foreground .icon-star-half"))
        context_ratings.append(full_start + half_start*0.5)
     
        number_reviews = item(".listing-location").text()
        index = re.search("\d", number_reviews)
        if index:
            i = index.start()
            room_type = number_reviews[:i]
            number_reviews = number_reviews[i: ]

            number_reviews = int(number_reviews[: number_reviews.index(" ")])            
            room_type = room_type[:room_type.index("\n")].replace(" ","_")
        else:
            room_type = number_reviews.replace(" ","_")
            number_reviews = 0

        context_number_of_reviews.append(number_reviews)
        context_room_type.append(room_type)
        
        room_id = item(".listing-img > a").attr.href        
        room_id = int(room_id[7:room_id.index("?")])
        context_room_id.append(room_id)
        
        host_id = item(".card-profile-picture").attr.href
        host_id = int(host_id[12:])
        context_host_id.append(room_id)
        
        if len(item(".icon-instant-book")) == 0:
            context_instant_booking.append(False)
        else:
            context_instant_booking.append(True)
        
        if len(item((".superhost-photo-badge")) == 0 ):
            context_super_hosts.append(False)
        else:
            context_super_hosts.append(True)
        #context_new_items
        
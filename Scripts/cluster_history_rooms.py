import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import cluster, datasets
from sklearn.neighbors import kneighbors_graph
from sklearn.preprocessing import StandardScaler

path="/home/raulsanchez/recsys_airbnb/"
world = pd.read_csv(path+'datasets/world_v1.5.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)
colors = np.array([x for x in 'bgrcmykbgrcmykbgrcmykbgrcmyk'])
colors = np.hstack([colors] * 20)
features = ["number_of_reviews","expensiveness","confortness","context_kurtosis","context_skew"]

have_neightbors = test_cases[test_cases.valid_neighbor_users_length>0]
have_neightbors_and_airbnb_ranking = have_neightbors[have_neightbors.airbnb_ranking_length > 0]

users_centroid = pd.DataFrame(columns=features)
for index, test_case in have_neightbors_and_airbnb_ranking.iterrows():
    #test_case = have_neightbors_and_airbnb_ranking.iloc[0]
    test_case.target_guest_history
    user_centroid = world[world.id.isin(test_case.target_guest_history)][features].mean()
    user_centroid.name = test_case.test_case_id
    users_centroid = users_centroid.append(user_centroid)

users_centroid = users_centroid.dropna()
X = users_centroid.values
X = StandardScaler().fit_transform(X)

bandwidth = cluster.estimate_bandwidth(X, quantile=0.3)
ms = cluster.MeanShift(bandwidth=bandwidth, bin_seeding=True)
affinity_propagation = cluster.AffinityPropagation(damping=.9,preference=-200)

users_centroid["ms_label"] = [0]*len(users_centroid)
users_centroid["af_label"] = [0]*len(users_centroid)
label_result = ["ms_label", "af_label"]
for index, algorithm in enumerate([ms, affinity_propagation]):
    algorithm.fit(X)
    users_centroid[label_result[index]] = algorithm.labels_
    
for index, result in enumerate(label_result):
    clusters = users_centroid[result].unique()
    clusters.sort()
    for i_cluster in clusters:
        cluster = users_centroid[users_centroid[result] == i_cluster].index.values
        test_cases[test_cases.test_case_id.isin(cluster)]
    

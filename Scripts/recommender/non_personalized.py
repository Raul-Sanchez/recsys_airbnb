def non_personalized(feature, ascending, experiment):
    #feature, ascending, experiment =("price", 0, 2)
    import time
    import pandas as pd
    import pickle    
    pd.options.mode.chained_assignment = None 
        
    start_time = time.time()
    path="/home/raulsanchez/recsys_airbnb/"
    test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

    if(experiment == 1):
        cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
        test_cases.rooms_around_target_500m = test_cases.rooms_around_target_500m.apply(eval)
    else:
        ranking_listing = pd.read_csv(path+'datasets/ranking_listings_v1.3(num_of_rev).csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
        test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)

    qrel_columns = ["query_id","iter", "doc_id", "rank"]
    result_columns = ["query_id","iter", "doc_id", "rank", "sim", "run_id"]
    qrel = pd.DataFrame(columns=qrel_columns)
    results = pd.DataFrame(columns=result_columns)

    have_neightbors = test_cases[test_cases.valid_neighbor_users_length>0]
    have_neightbors_and_airbnb_ranking = have_neightbors[have_neightbors.airbnb_ranking_length > 0]

    start_time = time.time()
    for index, test_case in have_neightbors_and_airbnb_ranking.iterrows():
        #test_case = have_neightbors_and_airbnb_ranking[have_neightbors_and_airbnb_ranking.test_case_id == "NYC-1215"].iloc[0]
        if(experiment == 1):
            rooms_around_target = cities[cities.id.isin(test_case.rooms_around_target_500m)]
        else:
            rooms_around_target = ranking_listing[ranking_listing.id.isin(test_case.airbnb_ranking)]

        rooms_around_target[feature] = rooms_around_target[feature].fillna(0)
        rooms_around_target = rooms_around_target.sort_values(by=feature, ascending=ascending)
        
        qrel_local = pd.DataFrame( [[0,0,0,1]] ,columns=qrel_columns)
        qrel_local.query_id = test_case.test_case_id
        qrel_local.doc_id = test_case.target_room
        
        results_local = pd.DataFrame( [[0]*len(result_columns)]*len(rooms_around_target), columns=result_columns)
        results_local.query_id = [test_case.test_case_id] * len(results_local)
        results_local.sim = rooms_around_target[feature].values
        results_local.doc_id = rooms_around_target.id.values
        results_local["rank"] = list(results_local.index.values +1)
        results_local.run_id = ["exp_"+str(experiment)+"_non_personalized_"+feature]*len(results_local)
        
        qrel = pd.concat([qrel,qrel_local])
        results = pd.concat([results,results_local])

    qrel.to_csv(path+"results/exp_"+str(experiment)+".1_qrel_non_personalized_"+feature, sep="\t",header=False, index=False)
    results.to_csv(path+"results/exp_"+str(experiment)+".1_non_personalized_"+feature, sep="\t",header=False, index=False)

    elapsed_time = time.time() - start_time
    print(elapsed_time)


non_personalized("number_of_reviews", 0, 1)
non_personalized("review_scores_rating",0, 1)
non_personalized("price",1, 1)

non_personalized("number_of_reviews", 0, 2)
non_personalized("review_scores_rating",0, 2)
non_personalized("price",1, 2)
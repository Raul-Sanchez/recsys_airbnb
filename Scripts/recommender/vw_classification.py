experiment = 1;
import os
import re
import time
import pickle
import subprocess
import numpy as np
import pandas as pd
from nltk.corpus import stopwords
cachedStopWords = stopwords.words("english")
def clean_stop_words(text):
    return(' '.join([word for word in text.split() if word not in cachedStopWords]))
pd.options.mode.chained_assignment = None

start_time = time.time()
path="/home/raulsanchez/recsys_airbnb/"
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.rooms_around_target_500m = test_cases.rooms_around_target_500m.apply(eval)
test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)

world = pd.read_csv(path+'datasets/world_v1.4.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

if(experiment == 1):
    cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
else:
    cities = pd.read_csv(path+'datasets/ranking_listings_v1.3(num_of_rev).csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

cities.bathrooms = cities.bathrooms.astype(float)
cities["price_value"] = (cities.price - cities.price.mean())/((cities.bedrooms+cities.bathrooms)/2)
world["price_value"] = (world.price - world.price.mean())/((world.bedrooms+world.bathrooms)/2)
#cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

qrel_columns = ["query_id","iter", "doc_id", "rank"]
result_columns = ["query_id","iter", "doc_id", "rank", "sim", "run_id"]
qrel = pd.DataFrame(columns=qrel_columns)
results = pd.DataFrame(columns=result_columns)

have_neightbors = test_cases[test_cases.valid_neighbor_users_length>0]
have_neightbors_and_airbnb_ranking = have_neightbors[have_neightbors.airbnb_ranking_length > 0]

space_f = ["accommodates", "bathrooms", "bedrooms", "beds"]
price_f = ["price"]
rating_f = ["number_of_reviews","review_scores_rating"]
text_f = ['name', "summary", "space", "description", "experiences_offered", "neighborhood_overview", "notes", "transit"]

property_type_f = set(world.property_type.str.strip().str.replace(" ", "_").fillna("property_type_NaN").unique())
property_type_f.update( cities.property_type.str.strip().str.replace(" ", "_").fillna("property_type_NaN").unique() )
property_type_f =list(property_type_f)

room_type_f = set(world.room_type.str.strip().str.replace(" ", "_").fillna("room_type_NaN").unique())
room_type_f.update( cities.room_type.str.strip().str.replace(" ", "_").fillna("room_type_NaN").unique() )
room_type_f=list(room_type_f)

features = price_f + room_type_f + space_f + ["price_value"]
start_time = time.time()
i=0
for index, test_case in have_neightbors_and_airbnb_ranking.iloc[:10].iterrows():
    print( i )
    i+=1
    #test_case = have_neightbors_and_airbnb_ranking.iloc[1232]; #test_case.target_room
    if(experiment == 1):
        test = cities[cities.id.isin(test_case.rooms_around_target_500m)]
    else:
        test = cities[cities.id.isin(test_case.airbnb_ranking)]
    
    target_city = test_case.target_room[0:3]
    target_city = cities[~cities.id.isin(test.id)]
    
    #remind me to add items from cities to world whenever they are on someones history records
    train = cities[cities.id.isin(test_case.target_guest_history)]
    train2 = world[world.id.isin(test_case.target_guest_history)]
    train = train.append(train2[~train2.id.isin(train.id)])
    rooms_not_in_train = cities[~cities.id.isin(test.id)]
    random_sampling = np.random.random_integers(low=0,high=len(rooms_not_in_train),size=len(train)*2)
    train = train.append( rooms_not_in_train.iloc[random_sampling])

    train["vw_format"] = ["1 |f "]*len(test_case.target_guest_history)+["-1 |f "]*len(random_sampling)
    test["vw_format"] = (test.id == test_case.target_room).astype(int).astype(str).str.replace("0","-1")+" '"+ test.id +" | "
    train[features] = train[features].fillna("0")
    test[features] = test[features].fillna("0")
    i=0
    for f in features:
        f_str = re.sub(r'[^\w]', ' ', f.lower() )
        f_str = f_str.replace(" ","").replace("_","").strip()
        #f_str = str(i)
        strs = f_str+":"+train[f].astype(str)+" "
        strs = strs.replace("|","")
        train.vw_format = train.vw_format.str[:] + strs
        
        strs = f_str+":"+test[f].astype(str)+" "
        strs = strs.replace("|","")
        test.vw_format = test.vw_format.str[:] + strs
        i+=1

    train[["vw_format"]].to_csv(path+"vowpal_wabbit/vw_train", header=False, index=False)
    test[["vw_format"]].to_csv(path+"vowpal_wabbit/vw_test", header=False, index=False)
    os.remove(path+"vowpal_wabbit/vw_train_cache")
    vw_command = ["vw",
    "-d",
    path+"vowpal_wabbit/vw_train",
    "--cache_file",
    path+"vowpal_wabbit/vw_train_cache",
    "-f",
    path+"vowpal_wabbit/vw_regressor",
    "--passes",
    "1000",
    "--loss_function",
    "logistic",
    ]
    out = subprocess.check_output(vw_command, stderr=subprocess.STDOUT)
    out = str(out).replace("\\n"," \n")
    print(out)
    vw_command = ["vw",
    "-d",
    path+"vowpal_wabbit/vw_test",
    "-i",
    path+"vowpal_wabbit/vw_regressor",
    "-p",
    path+"vowpal_wabbit/vw_results",
    "--loss_function",
    "logistic",
    ]
    out2 = subprocess.check_output(vw_command, stderr=subprocess.STDOUT)
    out2 = str(out2).replace("\\n"," \n")
    #print(out2)
    #print(test_case.target_room)
    #test_case.target_room
    results_file = pd.read_csv(path+"vowpal_wabbit/vw_results", sep=" ", header=-1)
    results_file.columns = ["sim","id"]

    qrel_local = pd.DataFrame( [[0,0,0,1]] ,columns=qrel_columns)
    qrel_local.query_id = test_case.test_case_id
    qrel_local.doc_id = test_case.target_room
    
    results_local = pd.DataFrame( [[0]*len(result_columns)]*len(test), columns=result_columns)
    results_local.query_id = [test_case.test_case_id] * len(results_local)
    results_local.sim = results_file.sim.values
    results_local.doc_id = results_file.id.values
    results_local = results_local.sort_values(by=["sim"], ascending=0)
    results_local["rank"] = list(range(1, len(results_local)+1))
    results_local.run_id = ["exp_"+str(experiment)+"_logistic_regression"]*len(results_local)
    print(results_local[results_local.doc_id == test_case.target_room]["rank"].values[0])
    
    
    
    
    
    
    
    
    
    
    
    qrel = pd.concat([qrel,qrel_local])
    results = pd.concat([results,results_local])

    os.remove(path+"vowpal_wabbit/vw_train")
    os.remove(path+"vowpal_wabbit/vw_regressor")
    os.remove(path+"vowpal_wabbit/vw_test")
    os.remove(path+"vowpal_wabbit/vw_results")
    #os.remove(path+"vowpal_wabbit/vw_scores")
qrel.to_csv(path+"results/exp_"+str(experiment)+".1_qrel_non_personalized_"+feature, sep="\t",header=False, index=False)
results.to_csv(path+"results/exp_"+str(experiment)+".1_non_personalized_"+feature, sep="\t",header=False, index=False)

elapsed_time = time.time() - start_time
print(elapsed_time)
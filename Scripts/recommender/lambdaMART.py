#experiment =2
import os
import time
import pickle
import subprocess
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None

start_time = time.time()
path="/home/raul/recsys_airbnb/"
city_codes = ["001", "002", "003", "004", "005"]
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)#test_cases.target_guest_history.iloc[0][0]
test_cases.valid_neighbor_users = test_cases.valid_neighbor_users.apply(eval)
test_cases.target_guest_history_dates = test_cases.target_guest_history_dates.apply(lambda x: pd.to_datetime( eval(x))) #test_cases.target_guest_history_dates[0]
test_cases.date = test_cases.date.apply(lambda x: pd.to_datetime([x]))
test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)
test_cases.rooms_around_target_500m = test_cases.rooms_around_target_500m.apply(eval)

user_item_matrix = pd.read_csv(path+'datasets/user_item_matrix.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
user_item_matrix.date = pd.to_datetime(user_item_matrix.date) #user_item_matrix.date[0]

if(experiment == 1):
    cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
else:
    ranking_listing = pd.read_csv(path+'datasets/ranking_listings_v1.3(num_of_rev).csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

qrel_columns = ["query_id","iter", "doc_id", "rank"]
result_columns = ["query_id","iter", "doc_id", "rank", "sim", "run_id"]
qrel = pd.DataFrame(columns=qrel_columns)
results = pd.DataFrame(columns=result_columns)

have_neightbors = test_cases[test_cases.valid_neighbor_users_length>0]
have_neightbors_and_airbnb_ranking = have_neightbors[have_neightbors.airbnb_ranking_length > 0]

have_neightbors_and_airbnb_ranking["date_str"] = have_neightbors_and_airbnb_ranking.date.apply(lambda x: x.strftime('%Y-%m-%d')[0])

user_item_matrix.user_item = "1"+user_item_matrix.user_item.str[1:]
for date in have_neightbors_and_airbnb_ranking.date_str.unique():
    #date = have_neightbors_and_airbnb_ranking.date_str.unique()[3]
    test_cases_by_date = have_neightbors_and_airbnb_ranking[have_neightbors_and_airbnb_ranking.date_str == date]
    user_item_matrix_local = user_item_matrix[user_item_matrix.date < pd.to_datetime([date])[0]]
    
    #We will give an importance of 1 to the items that are on the guests history
    for index, test_case in test_cases_by_date.iterrows():
        #test_case = test_cases_by_date.iloc[0]
        for room in test_case.target_guest_history:
            #room = test_case.target_guest_history[0]
            user_item_matrix_local_room = user_item_matrix_local[(user_item_matrix_local.user == test_case.target_guest) & (user_item_matrix_local.item == room)]
            user_item_matrix_local_room.user_item = "2"+user_item_matrix_local_room.user_item.str[1:]
            user_item_matrix_local[(user_item_matrix_local.user == test_case.target_guest) & (user_item_matrix_local.item == room)] = user_item_matrix_local_room

    user_item_matrix_local.user_item.to_csv(path+"vowpal_wabbit/vw_train_faster_"+str(experiment), index=False, header=False)
    '''test_case = test_cases_by_date.iloc[2]
    test_case.target_room
    test_case.target_guest
    user_item_matrix_local[user_item_matrix_local.user == test_case.target_guest].item
    user_item_matrix_local[user_item_matrix_local.item == test_case.target_room].user'''
    
    #TRAIN
    vw_command =["vw","-d",path+"vowpal_wabbit/vw_train_faster_"+str(experiment),"-b","18","-q","ui","--rank","10","--l2","0.001",
    "--learning_rate","0.015","--passes","20","--decay_learning_rate","0.97","--power_t","0","-f",
    path+"vowpal_wabbit/vw_regressor_faster_"+str(experiment)+".reg","--cache_file",path+"vowpal_wabbit/vw_train_faster_"+str(experiment)+".cache"]
    
    out1 = subprocess.check_output(vw_command, stderr=subprocess.STDOUT)
    out1 = str(out1).replace("\\n"," \n")#
    os.remove(path+"vowpal_wabbit/vw_train_faster_"+str(experiment)+".cache")
    os.remove(path+"vowpal_wabbit/vw_train_faster_"+str(experiment))
    print(out1)
    
    for index, test_case in test_cases_by_date.iterrows():
        start_time_local = time.time()
        #test_case = test_cases_by_date.iloc[200]
        print(index)       
        if(experiment == 1):
            rooms_around_target = "0 |u "+str(test_case.target_guest)+" |i "+pd.Series(test_case.rooms_around_target_500m).str[:]
        else:
            rooms_around_target = "0 |u "+str(test_case.target_guest)+" |i "+pd.Series(test_case.airbnb_ranking).str[:]
        #len(test_case.rooms_around_target_500m)
        #len(rooms_around_target)
        target_room_row = rooms_around_target[rooms_around_target.str.contains(test_case.target_room)].values[0]
        rooms_around_target[rooms_around_target.str.contains(test_case.target_room)] = "1"+target_room_row[1:]
        rooms_around_target.to_csv(path+"vowpal_wabbit/vw_test_faster_"+str(experiment), index=False, header=False)

        #TEST
        vw_command = ["vw","-d", path+"vowpal_wabbit/vw_test_faster_"+str(experiment), "-i", 
                      path+"vowpal_wabbit/vw_regressor_faster_"+str(experiment)+".reg","-p", path+"vowpal_wabbit/vw_results_faster_"+str(experiment) ,"-t"]
        out2 = subprocess.check_output(vw_command, stderr=subprocess.STDOUT)
        out2 = str(out2).replace("\\n"," \n")
        print(out2)
        qrel_local = pd.DataFrame( [[0,0,0,1]] ,columns=qrel_columns)
        qrel_local.query_id = test_case.test_case_id
        qrel_local.doc_id = test_case.target_room

        results_local = pd.DataFrame( [[0]*len(result_columns)]*len(rooms_around_target), columns=result_columns)
        results_local.query_id = [test_case.test_case_id] * len(results_local)
        results_local.sim = pd.read_csv(path+"vowpal_wabbit/vw_results_faster_"+str(experiment), header=-1).values
        results_local.doc_id = rooms_around_target.apply(lambda x: x[x.index("|i")+3:])
        results_local.sort_values(by=["sim"], ascending=False, inplace=True)
        results_local["rank"] = list(range(1,len(results_local)+1))
        results_local.run_id = ["exp_"+str(experiment)+"_matrix_factorization_faster_0_1_2"]*len(results_local)
        #results_local[results_local.doc_id == test_case.target_room]
        qrel = pd.concat([qrel,qrel_local])
        results = pd.concat([results,results_local])
        elapsed_time_local = time.time() - start_time_local
        #print(test_case.test_case_id+"//////////////////"+str(elapsed_time_local)+out1+out2)
        #print(test_case.test_case_id+"//////////////////"+str(elapsed_time_local))
    os.remove(path+"vowpal_wabbit/vw_regressor_faster_"+str(experiment)+".reg")
    os.remove(path+"vowpal_wabbit/vw_test_faster_"+str(experiment))
    os.remove(path+"vowpal_wabbit/vw_results_faster_"+str(experiment))

qrel.to_csv(path+"results/exp_"+str(experiment)+".5_qrel_matrix_factorization_faster_0_1_2", sep="\t",header=False, index=False)
results.to_csv(path+"results/exp_"+str(experiment)+".5_matrix_factorization_faster_0_1_2", sep="\t",header=False, index=False)

elapsed_time = time.time() - start_time
print(elapsed_time)

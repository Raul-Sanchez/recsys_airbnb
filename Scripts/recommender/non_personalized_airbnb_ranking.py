feature, ascending, experiment =("price", 0, 2)
import time
import pandas as pd
import pickle    
pd.options.mode.chained_assignment = None 
    
path="/home/raulsanchez/recsys_airbnb/"
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

ranking_listing = pd.read_csv(path+'datasets/ranking_listings_v1.3(num_of_rev).csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)

qrel_columns = ["query_id","iter", "doc_id", "rank"]
result_columns = ["query_id","iter", "doc_id", "rank", "sim", "run_id"]
qrel = pd.DataFrame(columns=qrel_columns)
results = pd.DataFrame(columns=result_columns)

have_neightbors = test_cases[test_cases.valid_neighbor_users_length>0]
have_neightbors_and_airbnb_ranking = have_neightbors[have_neightbors.airbnb_ranking_length > 0]

start_time = time.time()
for index, test_case in have_neightbors_and_airbnb_ranking.iterrows():
    #test_case = have_neightbors_and_airbnb_ranking[have_neightbors_and_airbnb_ranking.test_case_id == "NYC-1215"].iloc[0]
    rooms_around_target = ranking_listing[ranking_listing.id.isin(test_case.airbnb_ranking)]
    #len(rooms_around_target)
    #len( pd.Series(test_case.airbnb_ranking).value_counts() )
    #test_case
    rooms_around_target[feature] = rooms_around_target[feature].fillna(0)
    
    existing_rooms_around_target = rooms_around_target.id.values
    rooms_around_target = list()
    
    for airbnb_room in test_case.airbnb_ranking:
        if( airbnb_room in existing_rooms_around_target ):
            rooms_around_target.append(airbnb_room)
    ##len(rooms_around_target) 
    #len(pd.Series(rooms_around_target).unique())

    #len(test_case.airbnb_ranking)
    #len(pd.Series(test_case.airbnb_ranking).unique())
    qrel_local = pd.DataFrame( [[0,0,0,1]] ,columns=qrel_columns)
    qrel_local.query_id = test_case.test_case_id
    qrel_local.doc_id = test_case.target_room
    
    results_local = pd.DataFrame( [[0]*len(result_columns)]*len(rooms_around_target), columns=result_columns)
    results_local.query_id = [test_case.test_case_id] * len(results_local)
    results_local["rank"] = list(results_local.index.values +1)
    results_local.sim = list( (float(1)/len(rooms_around_target))*results_local["rank"] )[::-1]
    results_local.doc_id = rooms_around_target
    results_local.run_id = ["exp_"+str(experiment)+"_non_personalized_airbnb"]*len(results_local)
    
    qrel = pd.concat([qrel,qrel_local])
    results = pd.concat([results,results_local])

qrel.to_csv(path+"results/exp_"+str(experiment)+".4_qrel_non_personalized_airbnb", sep="\t",header=False, index=False)
results.to_csv(path+"results/exp_"+str(experiment)+".4_non_personalized_airbnb", sep="\t",header=False, index=False)

elapsed_time = time.time() - start_time
print(elapsed_time)
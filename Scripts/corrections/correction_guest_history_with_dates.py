import pandas as pd
import pandas as pd
import numpy as np
import pickle
from pyquery import PyQuery as pq

path ='/home/raul/airbnb2/'
city_labes = city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
months = ["January", "February", "March","April","May","June","July", "August", "September", "October", "November", "December"]
city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx")
test_cases = pd.read_csv(path+'datasets/test_cases_v1.1.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)


new_history=list()
target_guest_history_dates=list()
for df_index, row in test_cases.iterrows():    
    print(df_index)    
    #row = test_cases[test_cases.test_case_id == 'NYC-1084'].iloc[0]
    target_room = row.target_room
    target_host = cities[cities.id == target_room].host_id.values[0]
    target_guest = str(row.target_guest)
    city = city_folders[city_labels.index(row.city)]

    guest_profile = pq(filename="/home/raul/airbnb/"+city +"/guests/"+target_guest+".html")
    
    reviews =  guest_profile(".reviews .avatar-wrapper")
    history = list()
    dates = list()
    for review in reviews:
        url = pq(review)("a").attr("href")
        url = url.replace("/users/show/","")
        history.append(int(url))
        
        date_str = pq(review)(".date").text()
        
        if(len(date_str) > 0 ):
            month = str(months.index( date_str[:date_str.index("2")-1] )+1)
            if(len(month) == 1):
                month = "0"+month
            year = date_str[date_str.index("2"):]
            dates.append( year+"."+month+".01" )
        else:
            dates.append("")
    
    if( history.count(target_host) == 1):
        index = history.index(target_host) #index of where the target_host (room) is, everythin before this room is valid history
        valid_hist = history[index+1:] #remember this are host_id
        valid_dates = dates[index+1:] #remember this are host_id
        reconstructed = list()
        reconstructed_dates = list()
        j=0
        for host_id in valid_hist:
            #host_id = valid_hist[2]
            rooms = cities[cities.host_id == host_id ]
            if(len(rooms) == 1):
                reconstructed.append(rooms.id.values[0])
                reconstructed_dates.append(valid_dates[j])
            else:
                rooms = world[world.host_id == host_id ]
                if len(rooms) == 1:
                    reconstructed.append(rooms.id.values[0])
                    reconstructed_dates.append(valid_dates[j])
            j+=1
        new_history.append(list(reconstructed))
        target_guest_history_dates.append(list(reconstructed_dates))
    else:
        print("===got something we don't know===")
        print(target_room)
        print(target_host)
        print(history)
        print(target_guest)
        new_history.append([])

len(new_history)
len(target_guest_history_dates)
len(test_cases)
test_cases.columns.values

test_cases["target_guest_history"] = new_history
test_cases["target_guest_history_dates"] = target_guest_history_dates
test_cases.target_guest_history_length = test_cases.target_guest_history.apply(lambda x: len(x))
test_cases = test_cases[test_cases.target_guest_history_length > 0 ]
test_cases.to_csv(path+'datasets/test_cases_v1.2.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

test_cases[test_cases.test_case_id == "NYC-0618"].target_guest_history
test_cases[test_cases.test_case_id == "NYC-0618"].target_guest_history_dates
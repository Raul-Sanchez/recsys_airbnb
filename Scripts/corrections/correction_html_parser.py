import time
import numpy as np
import pandas as pd
from pyquery import PyQuery as pq

############################
##    ranking_listing.csv  #
############################
path = "/home/raul/airbnb2"
ranking_listings = pd.read_csv(path+"/datasets/ranking_listings.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]
ranking_listings.cancellation_policy = ranking_listings.cancellation_policy.str.strip()
ranking_listings.bed_type = ranking_listings.bed_type.str.strip()
ranking_listings.property_type = ranking_listings.property_type.str.strip()
ranking_listings.room_type = ranking_listings.room_type.str.strip()
ranking_listings.host_id = ranking_listings.host_url.str[len("/users/show/")+1:]

start_time = time.time()
#ranking_listings.cleaning_fee.value_counts().index.values
#test =np.random.randint(len(ranking_listings), size=10)
#for index, room in ranking_listings.iloc[test].iterrows():
for index, room in ranking_listings.iterrows():
    print(index)
    #room = ranking_listings.iloc[test[1]]
    city_str = city_folders[city_labels.index(room.city)]
    html = pq(filename="/media/raul/BACKUP/airbnb_rankings/"+city_str+"/rooms_new/"+str(room.id[4:])+".html")
    features = html(".row>.col-md-9 .row .col-md-6 > [data-reactid]")
    
    for f in features:
        text = pq(f).text().strip().lower()
        #print(text)
        
        if(text[0:6] == "weekly"):
            weekly_discount = (text[text.index(":")+1:]) 
            if("%" in weekly_discount):
                weekly_discount = int(weekly_discount[:-1])
                weekly_discount = room.price*((100-weekly_discount)*0.01)
                room.weekly_price = weekly_discount
        
        if(text[0:7] == "monthly"):
            monthly_discount = (text[text.index(":")+1:])
            if("%" in monthly_discount):
                monthly_discount = int(monthly_discount[:-1])
                monthly_discount = room.price*((100-monthly_discount)*0.01)
                room.monthly_price = monthly_discount
        if(text[0:8] == "check in"):
            check_in = (text[text.index(":")+1:]).strip()
            room.check_in = check_in
        if(text[0:9] == "check out"):
            check_out = (text[text.index(":")+1:]).strip()
            room.check_out = check_out

    if room.minimum_nights == " null ":
        html = pq(filename=path+city_str+"/rooms_new/"+str(room.id)+".html")
        for item in html(".details-section .col-md-9>.row>.col-md-6>strong"):
            text = pq(item).text()
            if("night" in text):
                minimum_nights = text[:text.index(" ")]
    ranking_listings.loc[index] = room
elapsed_time = time.time() - start_time
print(elapsed_time)
ranking_listings.to_csv(path+'/datasets/ranking_listings_hltml_correction.csv', sep="\t", header=ranking_listings.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
###################################
##   City.csv and world.csv      ##
###################################
import time
import numpy as np
import pandas as pd
from pyquery import PyQuery as pq


#NUMBER OF REVIEWS
import time
import numpy as np
import pandas as pd
from pyquery import PyQuery as pq
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]
path = "/home/raul/airbnb2"
world = pd.read_csv(path+"/datasets/world.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
error_rooms = list()
world.host_listings_count = world.number_of_reviews
for index, room in world.iterrows():
    #room = world[world.id == "SFO-2947903"].iloc[0]
    file = "/home/raul/airbnb/"+ city_folders[city_labels.index(room.id[:3])]+"/rooms/"+room.id[4:]+".html"
    html = pq(filename=file)
    number_of_reviews = html(".review-wrapper h4.text-center-sm").text().lower().strip()
    print("---------"+room.id)
    if(number_of_reviews != ""):
        if( "no reviews yet" in number_of_reviews):
            number_of_reviews = 0
        else:
            number_of_reviews = number_of_reviews[0:number_of_reviews.index(" ")]
            number_of_reviews = int(number_of_reviews)
    else:
        number_of_reviews = html("[itemprop='reviewCount']").text().lower().strip()
        if(number_of_reviews != ""):
            number_of_reviews = number_of_reviews[number_of_reviews.index(" ")+1:]
            number_of_reviews = number_of_reviews[:number_of_reviews.index(" ")]
            number_of_reviews = int(number_of_reviews)
        else:
            number_of_reviews = len(html(".row .review"))
            if( number_of_reviews == 0 ):
                error_rooms.append(room.id)
    room.number_of_reviews = number_of_reviews
    world.loc[index] = room
world.to_csv(path+'/datasets/world.csv', sep="\t", header=world.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

import time
import numpy as np
import pandas as pd
from pyquery import PyQuery as pq
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]
path = "/home/raul/airbnb2"
ranking_listings = pd.read_csv(path+"/datasets/ranking_listings.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
ranking_listings.host_listings_count = ranking_listings.number_of_reviews
error_rooms = list()
for index, room in ranking_listings.iterrows():
    #room = world[world.id == "SFO-2947903"].iloc[0]
    file = "/media/raul/BACKUP/airbnb_rankings/"+ city_folders[city_labels.index(room.id[:3])]+"/rooms_new/"+room.id[4:]+".html"
    html = pq(filename=file)
    number_of_reviews = html(".review-wrapper h4.text-center-sm").text().lower().strip()
    #print("---------"+room.id)
    if(number_of_reviews != ""):
        if( "no reviews yet" in number_of_reviews):
            number_of_reviews = 0
        else:
            number_of_reviews = number_of_reviews[0:number_of_reviews.index(" ")]
            number_of_reviews = int(number_of_reviews)
    else:
        number_of_reviews = html("[itemprop='reviewCount']").text().lower().strip()
        if(number_of_reviews != ""):
            number_of_reviews = number_of_reviews[number_of_reviews.index(" ")+1:]
            number_of_reviews = number_of_reviews[:number_of_reviews.index(" ")]
            number_of_reviews = int(number_of_reviews)
        else:
            print(room.id)
            number_of_reviews = len(html(".row .review"))
            if( number_of_reviews == 0 ):
                error_rooms.append(room.id)
    room.host_listings_count = number_of_reviews
    ranking_listings.loc[index] = room
room.number_of_reviews

ranking_listings.to_csv(path+'/datasets/ranking_listings.csv', sep="\t", header=ranking_listings2.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")


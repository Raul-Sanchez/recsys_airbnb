import time
import numpy as np
import pandas as pd

path="/home/raulsanchez/recsys_airbnb/"
world = pd.read_csv(path+'datasets/world_v1.5.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
test_cases = pd.read_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)

world_extra = cities[cities.id.isin(test_cases.iloc[0].target_guest_history)]
for index, test_case in test_cases.iloc[1:].iterrows():
    #test_case = test_cases.iloc[1]
    test_case_history_in_city = cities[cities.id.isin(test_case.target_guest_history)]
    if len(test_case_history_in_city )>0:
        world_extra = world_extra.append(test_case_history_in_city)

cities_ids = set(world_extra.id.unique())
world_ids = set( world[world.id.isin(cities_ids)].id )
cities_ids_not_in_world = list( cities_ids - world_ids )
world = world.append(cities[cities.id.isin(cities_ids_not_in_world)])
#world.context_mean
world.to_csv(path+'datasets/world_v1.6.csv', sep="\t", header=world.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
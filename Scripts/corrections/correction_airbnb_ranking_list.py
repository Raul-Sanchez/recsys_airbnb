import glob
import pandas as pd
import numpy as np
from pyquery import PyQuery as pq
pd.options.mode.chained_assignment = None

path ='/media/raul/BACKUP/airbnb_rankings/'
city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
city_folders = ["New_York","San_Francisco", "Los_Angeles","Chicago","Boston"]

rooms_histories = [ pd.Series( glob.glob(path+city_folders[0] +"/rankings/*") ),
                  pd.Series( glob.glob(path+city_folders[1] +"/rankings/*") ),
                  pd.Series( glob.glob(path+city_folders[2] +"/rankings/*") ),
                  pd.Series( glob.glob(path+city_folders[3] +"/rankings/*") ),
                  pd.Series( glob.glob(path+city_folders[4] +"/rankings/*") )]
test_cases = pd.read_csv('/home/raul/airbnb2/datasets/test_cases_v1.4.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
for index, test_case in test_cases.iloc[3430:].iterrows():
    print(index)
    target_room = test_case.target_room
    target_id = target_room[4:]
    city_folder = city_labels.index(test_case.city)
    rooms_history = rooms_histories[ city_folder ] 
    pages = pd.DataFrame(rooms_history[rooms_history.str.contains("/"+target_id+"-")].values, columns=["file"])
    pages["order"] = pages.file.str[pages.file.values[0].index("-")+1:-5].astype(int) #pages.values
    pages = pages.sort("order")

    airbnb_ranking = list()
    airbnb_ranking_last_page = list()
    airbnb_ranking_local = list()
    for p in pages.iloc[0:-1].file.values:
        #p = pages[9]
        ranking = pq(filename = p)
        listings = ranking(".listing .media-cover[target]")
        airbnb_ranking_local = list()
        for l in listings:
            #l = listings[0]
            room =pq(l)
            room_id = room("a").attr("href")
            room_id = room_id[7: room_id.index("?")]
            airbnb_ranking_local.append(int(room_id))
        #print( len( pd.Series(airbnb_ranking+airbnb_ranking_local).unique() ) == len(airbnb_ranking+airbnb_ranking_local) )
        airbnb_ranking = airbnb_ranking + airbnb_ranking_local
        
    p = pages.iloc[-1].file
    ranking = pq(filename = p)
    listings = ranking(".listing .media-cover[target]") #len(listings )
    airbnb_ranking_last_page= list()
    for l in listings:
        #l = listings[0]
        room =pq(l)
        room_id = room("a").attr("href")
        room_id = room_id[7: room_id.index("?")]
        if( int(room_id) not in airbnb_ranking_local ):
            airbnb_ranking_last_page.append(int(room_id))
    airbnb_ranking = airbnb_ranking + airbnb_ranking_last_page #len(airbnb_ranking_last_page)
    
    airbnb_ranking  = pd.Series(airbnb_ranking )
    airbnb_ranking  = test_case.city +"-"+airbnb_ranking.astype(str).str[:]
    test_case.airbnb_ranking = str(list(airbnb_ranking.values))

temp = test_cases.airbnb_ranking.apply(lambda x: len(eval(x)) )
test_cases.airbnb_ranking_length = temp.values
test_cases.to_csv('/home/raul/airbnb2/datasets/test_cases_v1.5.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
import time
import glob
import pandas as pd
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

city_str="Los_Angeles"
path = "/media/raul/BACKUP/airbnb_rankings/"
error_urls = pd.read_csv(path+city_str+"/error_urls.csv", header=-1)
rooms_history = pd.Series( glob.glob(path+city_str+"/error_rooms/*") )

if(len(rooms_history) >0):
    rooms_history = rooms_history.str[len(path+city_str)+13:-5]
    rooms_history = rooms_history.astype(int).values
else:
    rooms_history = []

error_urls = error_urls[~error_urls[0].isin(rooms_history)]
#error_urls.to_csv("/home/raul/LA_error_rooms.csv", index=False)
driver = webdriver.Firefox()
driver.get("https://www.airbnb.com")
def login( ):
	email  = "dabirolbd@gmail.com"
	passwd = "c5h1yyugr"
	driver.find_element_by_css_selector(".show-logout [href='/login']").click() #click on login
	time.sleep(4)
	driver.find_element_by_id("signin_email").send_keys(email)#set email
	driver.find_element_by_id("signin_password").send_keys(passwd)	#set password
	time.sleep(2)
	
	driver.find_element_by_id("user-login-btn").click()#login
	return
login()

len(error_urls[0])
len(error_urls[0].unique())

#for room in reversed(error_urls[0].unique()):
for room in error_urls[0].unique():
	print(room)
	room_url = "www.airbnb.com/rooms/"+str(room)
	driver.get(room_url)
	time.sleep(13)
	body = driver.find_element_by_css_selector("body").get_attribute('innerHTML')

	with open(path+city_str+"/error_rooms/"+ str(room) + ".html", 'w', encoding='utf-8') as f:
		print(body, file=f)
	time.sleep(2)
driver.close()
import os
import re
import time
import pickle
import subprocess
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None

start_time = time.time()
path="/home/raul/airbnb2/"
test_cases = pd.read_csv(path+'datasets/test_cases_v1.6.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

test_cases.target_guest_history = test_cases.target_guest_history.apply(lambda x: eval(x))
test_cases.target_guest_history = test_cases.target_guest_history.apply(lambda x: pd.Series(x).unique())
test_cases.target_guest_history_length = test_cases.target_guest_history.apply(len)

test_cases.target_guest_history = test_cases.target_guest_history.apply(lambda x: str(list(x)))
test_cases = pd.read_csv(path+'datasets/test_cases_v1.6.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.to_csv(path+'datasets/test_cases_v1.7.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
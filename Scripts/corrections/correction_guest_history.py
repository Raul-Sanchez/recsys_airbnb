import pandas as pd
import pandas as pd
import numpy as np
import pickle
from pyquery import PyQuery as pq

path ='/home/raul/airbnb2/'
city_labes = city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx")
test_cases = pd.read_csv(path+'datasets/test_cases.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]
p = "/home/raul/airbnb/"
i = 0

for label in city_labels:
    test_cases.target_guest_history = test_cases.target_guest_history.str.replace(label+"-","")
test_cases.target_guest_history = test_cases.target_guest_history.apply(lambda x: eval(x))

new_history=list()
for df_index, row in test_cases.iterrows():
    #print(df_index)
    #row = test_cases.iloc[123]    
    #row = test_cases[test_cases.target_room == "NYC-297698"]
    #row = test_cases[test_cases.test_case_id == 'NYC-0148']
    print(df_index)
    target_room = row.target_room
    target_host = cities[cities.id == target_room].host_id.values[0]
    target_guest = str(row.target_guest)
    city = city_folders[city_labels.index(row.city)]
    target_guest_history_old = row.target_guest_history #room_ids remembeeeeeeer!!!!!!!
    
    guest_profile = pq(filename=p+city +"/guests/"+target_guest+".html")
    reviews =  guest_profile(".reviews .avatar-wrapper")
    history = list()
    for review in reviews:
        url = pq(review)("a").attr("href")
        url = url.replace("/users/show/","")
        history.append(int(url))
    
    if( history.count(target_host) == 1):
        index = history.index(target_host) #index of where the target_host (room) is, everythin before this room is valid history
        valid_hist = history[index+1:] #remember this are host_id
        reconstructed = set()
        for host_id in valid_hist:
            rooms = cities[cities.host_id == host_id ]
            if len(rooms) == 1:
                reconstructed.add(rooms.id.values[0])                
            rooms = world[world.host_id == host_id ]
            if len(rooms) == 1:
                reconstructed.add(rooms.id.values[0])
        new_history.append(list(reconstructed))
    else:
        print("===got something we don't know===")
        print(target_room)
        print(target_host)
        print(history)
        print(target_guest)
        new_history.append([])

len(new_history)
len(test_cases)
test_cases.columns.values

test_cases["target_guest_history"] = new_history
test_cases.target_guest_history_length = test_cases.target_guest_history.apply(lambda x: len(x))
test_cases = test_cases[test_cases.target_guest_history_length > 0 ]
test_cases.to_csv(path+'test_cases_v1.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

test_cases[test_cases.target_guest_history_length > 0 ].city.value_counts()
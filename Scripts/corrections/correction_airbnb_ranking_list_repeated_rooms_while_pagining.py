import glob
import pandas as pd
import numpy as np
pd.options.mode.chained_assignment = None

test_cases = pd.read_csv('/home/raul/airbnb2/datasets/test_cases_v1.5.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(eval)

for index, test_case in test_cases.iterrows():
    #test_case = test_cases.iloc[4766]
    print(index)
    airbnb_ranking = pd.Series(test_case.airbnb_ranking)
    updated_airbnb_ranking = list()
    if( len(airbnb_ranking.unique()) != len(airbnb_ranking )):
        for room in airbnb_ranking.values:
            if(room not in updated_airbnb_ranking):
                updated_airbnb_ranking.append(room)
    else:
        updated_airbnb_ranking = pd.Series(test_case.airbnb_ranking)
    #print(test_case.airbnb_ranking)
    #print(updated_airbnb_ranking)
    test_case.airbnb_ranking = updated_airbnb_ranking
    test_case.airbnb_ranking_length = len(test_case.airbnb_ranking )
    test_cases.loc[index] = test_case

test_cases.airbnb_ranking = test_cases.airbnb_ranking.apply(lambda x: str(list(x)))
test_cases.to_csv('/home/raul/airbnb2/datasets/test_cases_v1.6.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
city_labels= ["NYC", "SFO", "LAX", "CHI", "BOS"]
proposed_f=["confortness","expensiveness"]
context_f = ["context_mean","context_min", "context_max", "context_histogram", "context_number_of_rooms","context_names", "context_prices", "context_ratings", "context_number_of_reviews", "context_room_type", "context_room_id", "context_host_id", "context_instant_booking", "context_super_hosts"]
city_folders= ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]

context_f = ["context_mean","context_min", "context_max", "context_histogram", "context_number_of_rooms","context_names", "context_prices", "context_ratings", "context_number_of_reviews", "context_room_type", "context_room_id", "context_host_id", "context_instant_booking", "context_super_hosts",
            "price_mean","price_std","price_centered_mu","price_centered_mean","price_normalized","context_skew","context_kurtosis"]
categorical_f = ["property_type","room_type", "bed_type"] # they are already dummy then we don't use them directly             
ordinal_f = ["host_response_time"]
space_f = ["accommodates", "bathrooms", "bedrooms", "beds"]
booking_f = ["minimum_nights", "host_acceptance_rate", "host_response_rate","guests_included"]
price_f = ["price", "weekly_price", "monthly_price", "security_deposit","cleaning_fee", "extra_people"]
rating_f = ["number_of_reviews","review_scores_rating","review_scores_accuracy","review_scores_cleanliness", "review_scores_checkin", "review_scores_communication", "review_scores_location", "review_scores_value"]
text_f = ['name', "summary", "space", "description", "experiences_offered", "neighborhood_overview", "notes", "transit"]
local_f = ["street","neighbourhood", "city"'state','zipcode','market','smart_location','country_code','country','latitude','longitude']
language_f = ["language"]

property_type_f = set(world.property_type.str.strip().str.replace(" ", "_").unique())
property_type_f.update( city.property_type.str.strip().str.replace(" ", "_").unique() )
property_type_f =list(property_type_f)

room_type_f = set(world.room_type.str.strip().str.replace(" ", "_").unique())
room_type_f.update( city.room_type.str.strip().str.replace(" ", "_").unique() )
room_type_f=list(room_type_f)

bed_type_f = set(world.bed_type.str.strip().str.replace(" ", "_").unique())
bed_type_f.update( city.bed_type.str.strip().str.replace(" ", "_").unique() )
bed_type_f=list(bed_type_f)

no_dummy_f = no_used_f+categorical_f+ordinal_f+space_f+booking_f+price_f+rating_f+text_f+local_f+language_f+property_type_f +room_type_f+bed_type_f+["bed_type_NaN","room_type_NaN","property_type_NaN"]
no_dummy_f  = city.columns[city.columns.isin(no_dummy_f)].unique()
amenities_f= city.columns[~city.columns.isin(no_dummy_f)].tolist()

no_used_f = ["host_since","host_location","host_about","host_is_superhost","host_verifications"
             ,"cancellation_policy","instant_bookable","reviews_per_month"            
             ,"id","listing_url","scrape_id","thumbnail",'last_scraped'
             ,'thumbnail_url', 'medium_url', 'picture_url', 'xl_picture_url','host_id'
             , 'host_url', 'host_name', 'host_thumbnail_url','host_picture_url'
             , 'host_neighbourhood', 'host_listings_count','host_total_listings_count'
             , 'host_has_profile_pic','host_identity_verified', 'neighbourhood_cleansed'
             ,'neighbourhood_group_cleansed', 'city', 'state','is_location_exact'
             , 'amenities', 'square_feet', 'calendar_updated','has_availability', 'availability_30'
             , 'availability_60','availability_90', 'availability_365', 'calendar_last_scraped','first_review'
             , 'last_review', 'requires_license', 'license','jurisdiction_names', 'require_guest_profile_picture'
             ,'require_guest_phone_verification','calculated_host_listings_count', 'maximum_nights']
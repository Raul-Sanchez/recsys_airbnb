import time
import pandas as pd
import numpy as np
import pickle    
pd.options.mode.chained_assignment = None 
path="/home/raul/airbnb2/"

test_cases = pd.read_csv(path+'datasets/test_cases_v1.6.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)#test_cases.target_guest_history.iloc[0][0]
test_cases.valid_neighbor_users = test_cases.valid_neighbor_users.apply(eval)
test_cases.target_guest_history_dates = test_cases.target_guest_history_dates.apply(lambda x: pd.to_datetime( eval(x))) #test_cases.target_guest_history_dates[0]
test_cases.date = test_cases.date.apply(lambda x: pd.to_datetime([x]))
#########################
# make_user_item_matrix #
#########################
target_guests = have_neightbors_and_airbnb_ranking.target_guest
user_item_matrix_columns = ["user","item","date", "user_item"]
user_item_matrix = pd.DataFrame(columns = user_item_matrix_columns )
start_time = time.time()
for target_guest in target_guests:
    #target_guest = target_guests.iloc[40]
    test_case = have_neightbors_and_airbnb_ranking[have_neightbors_and_airbnb_ranking.target_guest == target_guest]
    test_case = test_case[test_case.target_guest_history_length== test_case.target_guest_history_length.max()]
    target_room = test_case.target_room.values[0]
    date = test_case.date.values[0][0]
    last_date = date
    row = [target_guest, target_room, date, "0 |u "+str(target_guest)+" |i "+ target_room ]
    row = pd.DataFrame(row).T
    row.columns = user_item_matrix_columns
    user_item_matrix = pd.concat([user_item_matrix,  row], axis=0)

    i=0
    dates = test_case.target_guest_history_dates.values[0]
    
    min_date = dates.min()
    if(pd.isnull(min_date)):
        min_date = date

    dates = dates.fillna(min_date)

    for room in test_case.target_guest_history.values[0]:
        #room = test_case.target_guest_history.values[0][0]
        row = [target_guest, room, dates[i], "0 |u "+str(target_guest)+" |i "+ target_room ]
        row = pd.DataFrame(row).T
        row.columns = user_item_matrix_columns
        user_item_matrix = pd.concat([user_item_matrix,  row], axis=0)
        i+1
elapsed_time = time.time() - start_time
print(elapsed_time)
user_item_matrix.to_csv(path+'datasets/user_item_matrix.csv', sep="\t", header=user_item_matrix.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
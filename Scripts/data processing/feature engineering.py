import pandas as pd
import numpy as np
from scipy import stats
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.pyplot as plt
from pandas.tools.plotting import scatter_matrix

path = "/home/raul/recsys_airbnb/"
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
ranking_listings = pd.read_csv(path+'datasets/ranking_listings.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

'''for index, room in cities[cities.context_histogram.isnull()].iterrows():
    room.context_histogram = "[]"
    room.context_prices = "[]"
    world.loc[index] = room'''

#world.context_histogram = world.context_histogram.apply(lambda x: str(list(x) ) )
#world.context_prices = world.context_prices.apply(lambda x: str(list(x) ) )
datasets = [cities, ranking_listings]
dataset_label = ["cities.csv", "ranking_listings.csv"]
for i,dataset in enumerate(datasets):
    #i=1; dataset = datasets[i];
    print(i)
    dataset.context_histogram = dataset.context_histogram.apply(lambda x: eval(x) )
    dataset.context_prices = dataset.context_prices.apply(eval)
    dataset["price_mean"] = dataset.context_prices.apply(lambda x: pd.Series(x).mean() )
    dataset["price_std"] = dataset.price - dataset.context_prices.apply(lambda x: pd.Series(x).std() )
    dataset["price_centered_mu"] = dataset.price - dataset.context_mean
    dataset["price_centered_mean"] = dataset.price - dataset.price_mean
    dataset["price_normalized"] = (dataset.price_centered_mu / dataset.price_std).fillna(0)
    dataset["context_skew"] = dataset.context_histogram.apply(lambda x: stats.skew(x) )
    dataset["context_kurtosis"] = dataset.context_histogram.apply(lambda x: stats.kurtosis(x) )
    dataset["expensiveness"] = np.log((dataset.price/dataset.context_mean)).replace([np.inf, -np.inf], 0)
    dataset["confortness"] = (dataset.bedrooms+((dataset.beds + dataset.bathrooms)/2)) / (dataset.accommodates)
    
    dataset.context_histogram = dataset.context_histogram.apply(str)
    dataset.context_prices = dataset.context_prices.apply(str)

    dataset.to_csv(path+'datasets/'+dataset_label[i]+'.csv', sep="\t", 
                   header=dataset.columns.values, index=False, 
                   na_rep="NaNxxxx", encoding="utf-8")
###
scatter_matrix(cities[["expensiveness","confortness", "number_of_reviews","context_kurtosis","context_skew"]].fillna(0), alpha=0.2, figsize=(10, 10), diagonal='kde')
world.plot(kind="scatter", x="expensiveness", y="confortness", c="context_kurtosis", alpha=0.4, colormap="Spectral")

def merge_reviews_cities():
    import pandas as pd
    import glob
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    city_folders = ["New_York","San_Francisco", "Los_Angeles","Chicago","Boston"]
    world_reviews = pd.DataFrame()
    for c in city_folders:
        #c = city_folders[0]
        path = "/home/raul/airbnb/"+c+"/"
        world_reviews_local = pd.read_csv(path+'datasets/world_reviews.csv', sep="\t", header=0, na_values="NaNxxxx")
        world_reviews = world_reviews.append(world_reviews_local)
    world_reviews.to_csv("/home/raul/recsys_airbnb/datasets/cities_reviews.csv", sep="\t", header=cities_reviews.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

def build_raw_dataset_reviews(city_str):
    import pandas as pd
    import glob
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    city_folders = ["New_York","San_Francisco", "Los_Angeles","Chicago","Boston"]
    ranking_listings_reviews = pd.DataFrame()
    for c in city_folders:
        #c="Los_Angeles"
        print(c)
        path ='/media/raul/BACKUP/airbnb_only_html/'+c+'/rooms_new/reviews/'
        allFiles = glob.glob(path+"*.csv")

        for file in allFiles:
            df = pd.read_csv(file, sep="|", names = ["listing_id","id","date","reviewer_id","reviewer_name","comments"], index_col=False)
            df["city"] = [city_labels[city_folders.index(c)]]*len(df)
            ranking_listings_reviews = ranking_listings_reviews.append(df)
    ranking_listings_reviews.to_csv("/home/raul/recsys_airbnb/datasets/ranking_listing_reviews_raw.csv",
                                    sep="\t", header=ranking_listings_reviews.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

def clear_data():
    import pandas as pd
    path = "/home/raul/recsys_airbnb/"
    months = ["January", "February", "March","April","May","June","July", "August", "September", "October", "November", "December"]
    ranking_listings_reviews = pd.read_csv(path+'datasets/ranking_listing_reviews_raw.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
    ranking_listings_reviews["clean_date"] = ranking_listings_reviews.date.str.strip().str.split(" ")

    for index, row in ranking_listings_reviews.iloc[0:].iterrows():
        #row = ranking_listings_reviews.iloc[0]
        month_num = str(months.index( row.clean_date[0] )+1)
        if len(month_num) <= 1:
            month_num = "0"+str(month_num)
        row.clean_date = row.clean_date[-1]+"-"+month_num +"-01"
        ranking_listings_reviews.loc[index] = row
    
    ranking_listings_reviews.date = ranking_listings_reviews.clean_date
    ranking_listings_reviews = ranking_listings_reviews[ranking_listings_reviews.columns-["clean_date"]]
    ranking_listings_reviews.to_csv("/home/raul/recsys_airbnb/datasets/ranking_listing_reviews_v1.1.csv", header=ranking_listings_reviews.columns, index=False)

def add_sentiment_analisys(dataset_str):
    #dataset_str = "world_reviews.csv"
    import pandas as pd
    from nltk.sentiment.vader import SentimentIntensityAnalyzer
    path ="/home/raul/recsys_airbnb/"
    dataset = pd.read_csv(path+"datasets/"+dataset_str, sep="\t", encoding='utf-8')
    vader_analyzer = SentimentIntensityAnalyzer()
    dataset["sentiment"] = [""]*len(dataset)
    
    def get_sentiment(row):
        print(row.listing_id)
        return vader_analyzer.polarity_scores(row.comments)
        
    dataset["sentiment"] = dataset.apply(get_sentiment,axis=1)

    dataset.to_csv("/home/raul/recsys_airbnb/datasets/"+"cities_reviews.csv",
                   sep="\t", header=dataset.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
#dataset.iloc[2343].reviewer_id
import time
import numpy as np
import pandas as pd
pd.options.mode.chained_assignment = None 
start_time = time.time()
path = "/home/raulsanchez/recsys_airbnb/"
cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
world = pd.read_csv(path+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases = pd.read_csv(path+'datasets/test_cases_v1.2.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
test_cases.target_guest_history = test_cases.target_guest_history.apply(eval)
test_cases["valid_neighbor_users"] = [[]]*len(test_cases)
test_cases["valid_neighbor_users_length"] = [0]*len(test_cases)
test_cases["valid_neighbor_users_history_mean"] = [0]*len(test_cases)
test_cases.date = pd.to_datetime(test_cases.date)
test_cases.target_guest_history_dates = test_cases.target_guest_history_dates.apply(eval)
test_cases.target_guest_history_dates = test_cases.target_guest_history_dates.apply(pd.to_datetime)
i=0

for index, test_case in test_cases.iterrows():
    #test_case = test_cases[test_cases.target_guest_history_length>5].iloc[2]
    #test_cases[test_cases.target_guest == 26535495].target_guest_history_dates.values
    #test_case.target_guest
    #test_case[["target_guest_history", "target_guest_history_dates"]].values
    
    neighbor_users = test_cases[test_cases.target_room == test_case.target_room]
    neighbor_users = neighbor_users[neighbor_users.test_case_id != test_case.test_case_id]    
    #neighbor_users.date = pd.to_datetime(neighbor_users.date)
    #test_case.date = pd.to_datetime(test_case.date)
    valid_neighbor_users = neighbor_users[neighbor_users.date <= test_case.date]
    invalid_neighbor_users = neighbor_users[neighbor_users.date > test_case.date]
    
    if(len(invalid_neighbor_users) > 0):
        for index_inv, user in invalid_neighbor_users.iterrows():
            #user = invalid_neighbor_users.loc[5]
            user.target_guest_history
            #user.test_case_id
            #user.target_guest_history_dates = pd.to_datetime(eval(user.target_guest_history_dates))
            valid_items = user.target_guest_history_dates <= test_case.date
            
            user.target_guest_history_dates = user.target_guest_history_dates[valid_items]
            user.target_guest_history = pd.Series(user.target_guest_history)[valid_items].tolist()
            user.target_guest_history_length = len(user.target_guest_history)
            if(user.target_guest_history_length > 0):
                valid_neighbor_users = valid_neighbor_users.append(user)
    if(len(valid_neighbor_users )>0):
        test_case.valid_neighbor_users = valid_neighbor_users.target_guest.values
        test_case.valid_neighbor_users_length = len(test_case.valid_neighbor_users)
        test_case.valid_neighbor_users_history_mean = valid_neighbor_users.target_guest_history.apply(len).mean()
        valid_neighbor_users.target_guest_history.apply(len)            
        test_cases.loc[index] = test_case
    
    print(str(index)+"---------------")
test_cases.valid_neighbor_users = test_cases.valid_neighbor_users.apply(lambda x: str(list(x)) )
test_cases.date = test_cases.date.apply(lambda x: x.strftime('%Y-%m-%d'))
test_cases.target_guest_history_dates = test_cases.target_guest_history_dates.apply(lambda x: str(list(x.strftime('%Y-%m-%d'))))

test_cases.to_csv(path+'datasets/test_cases_v1.3(cf).csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
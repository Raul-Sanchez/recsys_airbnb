def merge_cities_world():
    import pandas as pd
    import numpy as np

    paths = [ '/home/raul/airbnb/New_York/','/home/raul/airbnb/San_Francisco/','/home/raul/airbnb/Los_Angeles/','/home/raul/airbnb/Chicago/','/home/raul/airbnb/Boston/']
    path = "/home/raul/airbnb2/datasets/"
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    city = pd.read_csv(p+"datasets/city.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
    city.id = city.id.astype(str)
    city.id = city_labels[0]+"-"+ city.id.str[:]
    i=1
    for p in paths[1:]:
        #p = paths[1]
        city_local = pd.read_csv(p+'datasets/city.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
        city_local.id = city_local.id.astype(str)
        city_local.id = city_labels[i]+"-"+city_local.id.str[:]
        not_in_city_local = list(set(city.columns) - set(city_local.columns))
        not_in_city = list(set(city_local.columns) - set(city.columns))
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_city)]*len(city) , columns = not_in_city,dtype = city_local[not_in_city].dtypes)
        city = city.join(new_columns)
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_city_local)]*len(city_local) , columns = not_in_city_local, dtype = city[not_in_city_local].dtypes)
        city_local =city_local.join(new_columns)
        
        city = city.append(city_local)
        #city = pd.concat([city,city_local])
        i+=1
    city.to_csv(path+'cities.csv', sep="\t", header=city.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")

    world = pd.read_csv(p+"datasets/world.csv", sep="\t", header=0, na_values="NaNxxxx",low_memory=False)    
    world.id = world.id.astype(str)
    world.id = city_labels[i]+"-"+ world.id.str[:]
    i=1
    for p in paths[1:]:
        #p = paths[1]
        world_local = pd.read_csv(p+'datasets/world.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)        
        world_local.id = world_local.id.astype(str)
        world_local.id = city_labels[i]+"-"+ world_local.id.str[:]        
        not_in_world_local = list(set(world.columns) - set(world_local.columns))
        not_in_world = list(set(world_local.columns) - set(world.columns))
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_world)]*len(world) , columns = not_in_world,dtype = world_local[not_in_world].dtypes)
        world = world.join(new_columns)
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_world_local)]*len(world_local) , columns = not_in_world_local, dtype = world[not_in_world_local].dtypes)
        world_local =world_local.join(new_columns)
        
        world = world.append(world_local)
        #city = pd.concat([city,city_local])
        i+=1
    world.to_csv(path+'world.csv', sep="\t", header=world.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
#######################
def create_test_cases():
    import glob
    import pandas as pd
    import numpy as np
    import pickle
    from pyquery import PyQuery as pq
    pd.options.mode.chained_assignment = None
    paths = [ '/home/raul/airbnb/New_York/','/home/raul/airbnb/San_Francisco/','/home/raul/airbnb/Los_Angeles/','/home/raul/airbnb/Chicago/','/home/raul/airbnb/Boston/']
    path = "/home/raul/airbnb2/datasets/"
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    columns = [
    "test_case_id", 
    "target_room", 
    "rooms_around_target",
    "rooms_around_target_length",
    "airbnb_ranking", 
    "airbnb_ranking_length",
    "target_guest", 
    "target_guest_history",
    "target_guest_history_length",
    "date"]
    i=0
    for p in paths:
        #p = paths[0]        
        test_cases = pd.read_csv(p+"datasets/N_ts.csv", sep="\t", header=0)
        test_cases_2 = pd.read_csv(p+"datasets/test_cases.csv", sep="\t", header=0)
        #temp = test_cases_2[test_cases_2.target_id == 3176874]
        #temp = temp[temp.guest_id == 9090718]
    
        N_ts_new =  pickle.load( open( p+"datasets/N_ts_new.csv", "rb" ) )
        N_ts_new.target_id = N_ts_new.target_id.astype(int).astype(str)
        N_ts_new.target_id = city_labels[i]+"-"+N_ts_new.target_id.str[:]

        for index, row in N_ts_new.iterrows():
            #row = N_ts_new.iloc[0]
            new_ranking = list()
            for room in row.aibnb_ranking:
                new_ranking.append(city_labels[i]+"-"+str(room))
            row.aibnb_ranking = new_ranking
            N_ts_new.loc[index] = row
        
        ids=list()
        for j in range(len(test_cases)):
            id=city_labels[i]+"-"+'{0:04}'.format(j)
            ids.append(id)
        test_cases["test_case_id"] = ids
            
        test_cases["target_room"] = test_cases.target_id.astype(int).astype(str)
        test_cases["target_room"] = city_labels[i]+"-"+test_cases.target_room.str[:]
        
        test_cases["rooms_around_target"] = test_cases["room_ids"].apply(lambda x: eval(x) )
        
        j = 0
        for neighbourhood in test_cases["rooms_around_target"]:
            #neighbourhood = test_cases["rooms_around_target"][0]
            neighbourhood = pd.Series(neighbourhood).astype(str)
            neighbourhood = city_labels[i]+"-"+neighbourhood.str[:]
            test_cases["rooms_around_target"][j] = neighbourhood
            j+=1
        test_cases["rooms_around_target"] = test_cases["rooms_around_target"].apply(lambda x: x.tolist() )

        test_cases["rooms_around_target_length"] = test_cases["rooms_around_target"].apply(lambda x: len(x))
        
        test_cases["airbnb_ranking"] = [[]]*len(test_cases)
        for index,test_case in test_cases.iterrows():
            #test_case = test_cases.iloc[0]
            airbnb_ranking = N_ts_new[N_ts_new.target_id == test_case.target_room].aibnb_ranking.values[0]
            if(test_case.target_room in airbnb_ranking):
                test_case.airbnb_ranking = airbnb_ranking
            test_cases.loc[index] = test_case
        
        test_cases["airbnb_ranking_length"] = test_cases.airbnb_ranking.apply(lambda x: len(x))
        
        city  = pd.read_csv(p+"datasets/city.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
        world = pd.read_csv(p+"datasets/world.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)

        test_cases["target_guest_history"] = [[]]*len(test_cases)
        for index, case_key in test_cases.iterrows():
            #case_key = test_cases.iloc[32]
            user_history = test_cases_2[test_cases_2.room_guest_date == case_key.room_guest_date].room_id
            city_history = set( city[city.id.isin(user_history)].id )
            world_history = set( world[world.id.isin(user_history)].id )
            world_history = world_history.difference(city_history)
            city_history = city[city.id.isin(city_history)]        
            world_history = world[world.id.isin(world_history)]
            user_history = city_history.append(world_history).id.values
            user_history_new = list()
            for room in user_history:
                user_history_new.append(city_labels[i]+"-"+str(room))
            case_key["target_guest_history"] = user_history_new
            test_cases.loc[index] = case_key
        
        test_cases["target_guest_history_length"] = test_cases["target_guest_history"].apply(lambda x: len(x))

        test_cases["date"] = test_cases.room_guest_date.str[-10:] +"T21:00:00.000000000-0300"
        test_cases["date"] = test_cases.date.apply(lambda x:pd.to_datetime(np.datetime64(x)).strftime('%Y.%m.%d'))
        
        test_cases.room_guest_date = test_cases.room_guest_date.apply(lambda x: x[x.index("-")+1:])
        test_cases.room_guest_date = test_cases.room_guest_date.apply(lambda x: x[0:x.index("-")])
        test_cases["target_guest"] = test_cases.room_guest_date
        
        test_cases = test_cases[columns]
        test_cases.to_csv(path+city_labels[i]+'-test_cases.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
        print(len(test_cases[test_cases.airbnb_ranking_length != 0 ])/len(test_cases))
        i+=1
def merge_test_cases():
    import pandas as pd
    paths = ['NYC-test_cases.csv','SFO-test_cases.csv','LAX-test_cases.csv','CHI-test_cases.csv','BOS-test_cases.csv']
    path = "/home/raul/airbnb2/datasets/"
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    test_cases = pd.read_csv(path+paths[0], sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
    test_cases["city"] = city_labels[0]
    i=1
    for p in paths[1:]:
        #p = paths[1]
        test_cases_local = pd.read_csv(path+p, sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
        test_cases_local["city"] = city_labels[i]
        test_cases = test_cases.append(test_cases_local)        
        i+=1
    test_cases = test_cases[test_cases.target_guest_history_length >0]
    
    for city in city_labels:
        #city = city_labels[0]
        by_city = test_cases[test_cases.city == city]
        ids=list()
        for j in range(len(by_city)):
            id=city+"-"+'{0:04}'.format(j)
            ids.append(id)
        by_city["test_case_id"] = ids    
        test_cases[test_cases.city == city] = by_city
    test_cases.to_csv(path+'test_cases.csv', sep="\t", header=test_cases.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
def create_500m_neighboorhood:
    import pandas as pd
    import numpy as np
    import geopy
    from geopy.distance import vincenty
    
    path = "/home/raul/airbnb2/"
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]    
    city_folders = ["New_York", "San_Francisco", "Los_Angeles", "Chicago", "Boston"]    
    
    test_cases = pd.read_csv(path+'datasets/test_cases.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
    N_ts = pd.read_csv('/home/raul/airbnb/'+city_folders[0]+'/datasets/N_ts.csv', sep="\t", header=0)
    N_ts["target_room"] = city_labels[0]+"-"+N_ts.target_id.astype(int).astype(str).str[:]
    
    i=1
    for label in city_folders[1:]:
        #label = city_folders[0]
        N_ts_local = pd.read_csv('/home/raul/airbnb/'+label+'/datasets/N_ts.csv', sep="\t", header=0)
        N_ts_local["target_room"] = city_labels[i]+"-"+N_ts_local.target_id.astype(int).astype(str).str[:]
        N_ts = pd.concat([N_ts,N_ts_local], axis=0)
        i+=1
    test_cases["rooms_around_target_1500m"] = [[]]*len(test_cases)
    test_cases["rooms_around_target_500m"] = [[]]*len(test_cases)
    for index, row in test_cases.iterrows():
        #row = test_cases.iloc[0]; index = 0
        rooms_around = N_ts[N_ts.target_room == row.target_room].room_ids.values[0]
        test_cases.loc[index, "rooms_around_target_1500m"] = rooms_around

    cities = pd.read_csv(path+'datasets/cities.csv', sep="\t", header=0, na_values="NaNxxxx")
    
    for index, test_case in test_cases.iterrows():
        #test_case = test_cases.iloc[0]; index = 0
        print(index)
        local_rooms_around_500m = list()
        local_rooms_around_1500m = list()
        #len(local_rooms_around_1500m)
        target_room = cities[cities.id == test_case.target_room]
        target_pt = geopy.Point(target_room.latitude.values[0], target_room.longitude.values[0] ) #target room
        rooms = eval(test_case.rooms_around_target_1500m)
        #len(rooms)
        for i in rooms:
            room_id = test_case.city+"-"+str(i)
            room = cities[cities.id == room_id]
            neighbor_pt = geopy.Point(room.latitude.values[0], room.longitude.values[0])
            dist = vincenty(target_pt , neighbor_pt ).km
            if(dist <= 0.5):
                local_rooms_around_500m.append(room_id)
            local_rooms_around_1500m.append(room_id)
        test_cases.loc[index, "rooms_around_target_1500m"] = str(local_rooms_around_1500m)        
        test_cases.loc[index, "rooms_around_target_500m"] = str(local_rooms_around_500m)
        #test_cases.rooms_around_target[0]
def build_raw_dataset(city_str):
    #city_str = "New_York"
    import pandas as pd
    import glob
    path ='/media/raul/BACKUP/airbnb_rankings/'+city_str+'/'
    
    top_header = ["id","listing_url","scrape_id","last_scraped","name","summary","space","description","experiences_offered","neighborhood_overview","notes","transit","thumbnail_url","medium_url","picture_url","xl_picture_url","host_id","host_url","host_name","host_since","host_location","host_about","host_response_time","host_response_rate","host_acceptance_rate","host_is_superhost","host_thumbnail_url","host_picture_url","host_neighbourhood","host_listings_count","host_total_listings_count","host_verifications","host_has_profile_pic","host_identity_verified","street","neighbourhood","neighbourhood_cleansed","neighbourhood_group_cleansed","city","state","zipcode","market","smart_location","country_code","country","latitude","longitude","is_location_exact","property_type","room_type","accommodates","bathrooms","bedrooms","beds","bed_type","amenities","square_feet","price","weekly_price","monthly_price","security_deposit","cleaning_fee","guests_included","extra_people","minimum_nights","maximum_nights","calendar_updated","has_availability","availability_30","availability_60","availability_90","availability_365","calendar_last_scraped","number_of_reviews","first_review","last_review","review_scores_rating","review_scores_accuracy","review_scores_cleanliness","review_scores_checkin","review_scores_communication","review_scores_location","review_scores_value","requires_license","license","jurisdiction_names","instant_bookable","cancellation_policy","require_guest_profile_picture","require_guest_phone_verification","calculated_host_listings_count","reviews_per_month", "check_out", "check_in"]
    #len(top_header)
    allFiles = glob.glob(path+"rooms_new/rooms/*.csv")
    world = pd.DataFrame()
    list_ = []
    for file_ in allFiles:
        df = pd.read_csv(file_, sep="|", names = top_header, index_col=False)
        list_.append(df)
    world = pd.concat(list_)
    world.index = world.id

    world.to_pickle(path+"datasets/world_rankings.csv")
def build_raw_dataset_error(city_str):
    #city_str = "Los_Angeles"
    import pandas as pd
    import glob
    
    path ='/media/raul/BACKUP/airbnb_rankings/'+city_str+'/'
    top_header = ["id","listing_url","scrape_id","last_scraped","name","summary","space","description","experiences_offered","neighborhood_overview","notes","transit","thumbnail_url","medium_url","picture_url","xl_picture_url","host_id","host_url","host_name","host_since","host_location","host_about","host_response_time","host_response_rate","host_acceptance_rate","host_is_superhost","host_thumbnail_url","host_picture_url","host_neighbourhood","host_listings_count","host_total_listings_count","host_verifications","host_has_profile_pic","host_identity_verified","street","neighbourhood","neighbourhood_cleansed","neighbourhood_group_cleansed","city","state","zipcode","market","smart_location","country_code","country","latitude","longitude","is_location_exact","property_type","room_type","accommodates","bathrooms","bedrooms","beds","bed_type","amenities","square_feet","price","weekly_price","monthly_price","security_deposit","cleaning_fee","guests_included","extra_people","minimum_nights","maximum_nights","calendar_updated","has_availability","availability_30","availability_60","availability_90","availability_365","calendar_last_scraped","number_of_reviews","first_review","last_review","review_scores_rating","review_scores_accuracy","review_scores_cleanliness","review_scores_checkin","review_scores_communication","review_scores_location","review_scores_value","requires_license","license","jurisdiction_names","instant_bookable","cancellation_policy","require_guest_profile_picture","require_guest_phone_verification","calculated_host_listings_count","reviews_per_month", "check_out", "check_in"]
    allFiles = pd.DataFrame(glob.glob(path+"error_rooms/*.html"))
    allFiles = allFiles[0].str[len(path+"/error_rooms/")-1:-5]
    #allFiles = allFiles[1:]
    #len(allFiles)
    world = pd.DataFrame()
    list_ = []
    for room in allFiles.values:
        df = pd.read_csv(path+"rooms_new/rooms/room_"+room+".csv", sep="|", names = top_header, index_col=False)
        world = world.append(df)
    world.index = world.id
    world.to_pickle(path+"/datasets/world_rankings_error.csv")
def clean_data(city_str):
    #city_str = "Los_Angeles"
    import pandas as pd
    import numpy as np
    import pickle
    path ='/media/raul/BACKUP/airbnb_rankings/'+city_str+'/'
    world =  pd.read_pickle(path+"datasets/world_rankings.csv")
    world2 =  pd.read_pickle(path+"datasets/world_rankings_error.csv")

    #world[world[ "weekly_price" ].str.contains("R").fillna(False)].id.to_csv(path+"error_urls.csv", index=False)
    #world[world.price.astype(str).str.contains("R")].id.to_csv(path+"error_urls3.csv", index=False)
    #world[world.cleaning_fee.str.contains("R").fillna(False)].id.to_csv(path+"error_urls4.csv", index=False)
    #world[world['security_deposit'].astype(str).str.contains("R")].id.to_csv(path+"error_urls5.csv", index=False)    
    
    world = world[~world.id.isin(world2.id.values)]
    world = world.append(world2)
    error = world[world[ "weekly_price" ].str.contains("R").fillna(False)]
    error2 = world[world[ "monthly_price" ].str.contains("R").fillna(False)]
    error = error.append(error2[~error2.id.isin(error.id)])
    error2 = world[world[ "price" ].str.contains("R").fillna(False)]
    error = error.append(error2[~error2.id.isin(error.id)])
    error2 = world[world[ "cleaning_fee" ].str.contains("R").fillna(False)]
    error = error.append(error2[~error2.id.isin(error.id)])
    error2 = world[world[ "security_deposit" ].str.contains("R").fillna(False)]
    error = error.append(error2[~error2.id.isin(error.id)])
    error2 = world[world["extra_people"].str.contains("R").fillna(False)]
    error = error.append(error2[~error2.id.isin(error.id)])
    
    world = world[~world.id.isin(error.id)]
    
    error_features=["weekly_price", "monthly_price", "price","cleaning_fee","security_deposit", "extra_people"]
    error[error_features] = error[error_features].astype(str)
    error[error_features] = error[error_features].apply(lambda x: x.str.replace("R", "") , axis=1)

    world = world.append(error)
    #error[features] = error[features].apply(lambda x: x.str.replace(" null ", "NaN") , axis=1)
    features = ["price", "weekly_price", "monthly_price", "security_deposit", "cleaning_fee"]
        
    world[ "weekly_price" ] = world[ "weekly_price" ].str.replace(" null ", "NaN")
    world[ "weekly_price" ] = (world[ "weekly_price" ].replace( '[\$,)]','', regex=True ).replace( '[(]','-',   regex=True ))
    world[ "weekly_price" ] = world[ "weekly_price" ].str.replace(" /week","")
    world[ "weekly_price" ] = world[ "weekly_price" ].astype(float)
    
    world[ "monthly_price" ] = (world[ "monthly_price" ].replace( '[\$,)]','', regex=True ).replace( '[(]','-',   regex=True ))
    world[ "monthly_price" ] = world[ "monthly_price" ].str.replace(' null ', "NaN")
    world[ "monthly_price" ] = world[ "monthly_price" ].str.replace(" /month","").astype(float)
    
    features = ["accommodates", "bathrooms", "bedrooms", "beds"]
    for f in features:
        world[f] = world[f].astype(str).str.replace("+","").astype(float)
    #world[features]

    world["minimum_nights"] = world["minimum_nights"].str.extract('(?P<digit>\d)').astype(float)

    features = ["price","cleaning_fee"]
    world['price'] = world['price'].astype(str)
    world = world[world['price'] != ' null ' ] #Item with price null generaly does not exist any longer
    world = world[world['cleaning_fee'] != ' null ' ] #Item with price null generaly does not exist any longer
    
    #world = world[~world.price.str.contains("R")]
    
    world[ features ] = (world[ features ].replace( '[\$,)]','', regex=True ).replace( '[(]','-',   regex=True ).astype(float))
    
    world[ "security_deposit" ] = world[ "security_deposit" ].replace( '[\$,)]','', regex=True )
    world[ "security_deposit" ] = world[ "security_deposit" ].astype(float)
        
    world["extra_people"] = world["extra_people"].str[0:4]
    world[ "extra_people" ] =world[ "extra_people" ].str.replace("/","")
    world[ "extra_people" ] =world[ "extra_people" ].str.replace("No","0")
    world["extra_people"] = (world["extra_people"].replace( '[\$,)]','', regex=True ).replace( '[(]','-',   regex=True ).astype(float))
    #world[world['extra_people'].astype(str).str.contains("R")].id.to_csv(path+"error_urls6.csv", index=False)
    features = ["host_acceptance_rate", "host_response_rate"]
    world[ features ] = world[ features ].replace( '[,%]','', regex=True ).astype(float)
    ##############################################################################
    #converting categorical into dummies
    world = world[~world["amenities"].isnull()]#remove nan values
    world["amenities"] = world["amenities"].str.strip()
    world["amenities"] = world["amenities"].str.replace("[","")
    world["amenities"] = world["amenities"].str.replace("]","")
    world["amenities"] = world["amenities"].str.replace("(","")
    world["amenities"] = world["amenities"].str.replace(")","")
    world["amenities"] = world["amenities"].str.replace("/","_")
    world["amenities"] = world["amenities"].str.replace(" ","_")
    world["amenities"] = world["amenities"].str.lower()
    world["amenities"] = world["amenities"].str.split(",")

    dummy_columns_w = pd.get_dummies(world["amenities"].apply(pd.Series).stack()).sum(level=0)
    world = pd.concat([world,dummy_columns_w], axis=1)
    
    categorical_f_w = []
    categorical_f = ["property_type","room_type", "bed_type"]
    for features in categorical_f:
        #features = categorical_f[0]        
        dummy_columns_w =  pd.get_dummies(world[features])
        dummy_columns_w.columns = dummy_columns_w.rename(columns=lambda x: x.strip()).columns.str.replace(" ","_")
        
        clean = dummy_columns_w.columns.tolist()
        if 'NaN' in clean:    
            clean[clean.index("NaN")] = features+"_NaN"
            dummy_columns_w.columns = clean
        
        world = pd.concat([world,dummy_columns_w], axis=1)
        
    world.number_of_reviews = world.number_of_reviews.astype(float)
    world.latitude = world.latitude.astype(float)
    world.longitude = world.longitude.astype(float)
    ##string sanity###
    for f in world.columns.values:
        if world[f].dtype == world["description"].dtype:            
            world[f] = world[f].str.replace("\r"," ")
    
    world[world.id.isin(error.id)][error_features] = (world[world.id.isin(error.id)][error_features]/(3.65))
    world.to_csv(path+'datasets/ranking_listings.csv', sep="\t", header=world.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
def merge_ranking_listing():
    import pandas as pd
    import numpy as np
    path = '/media/raul/BACKUP/airbnb_rankings'
    city_folders = [ '/New_York/','/San_Francisco/', '/Los_Angeles/','/Chicago/','/Boston/']    
    city_labels = ["NYC", "SFO", "LAX", "CHI", "BOS"]
    p = path+city_folders[0]
    city = pd.read_csv(p+"datasets/ranking_listings.csv", sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
    city.id = city.id.astype(str)
    city.id = city_labels[0]+"-"+ city.id.str[:]
    i=1
    for p in city_folders[1:]:
        #p = city_folders[1]
        city_local = pd.read_csv(path+p+'datasets/ranking_listings.csv', sep="\t", header=0, na_values="NaNxxxx", low_memory=False)
        city_local.id = city_local.id.astype(str)
        city_local.id = city_labels[i]+"-"+city_local.id.str[:]
        not_in_city_local = list(set(city.columns) - set(city_local.columns))
        not_in_city = list(set(city_local.columns) - set(city.columns))
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_city)]*len(city) , columns = not_in_city,dtype = city_local[not_in_city].dtypes)
        city = city.join(new_columns)
        
        new_columns = pd.DataFrame( [[np.nan]*len(not_in_city_local)]*len(city_local) , columns = not_in_city_local, dtype = city[not_in_city_local].dtypes)
        city_local =city_local.join(new_columns)
        
        city = city.append(city_local)#city.id
        #city = pd.concat([city,city_local])
        i+=1
    city["city"] = city.id.str[0:3]
    city.to_csv(path+'/ranking_listings.csv', sep="\t", header=city.columns.values, index=False, na_rep="NaNxxxx", encoding="utf-8")
    #city.city.value_counts()
    
   
#######################
    
####################
#merge_cities_world()
#create_test_cases()
#merge_test_cases()